
#include "GaussKernel2D.h"

f3 GaussKernel2D::gradW(f3 x_i, f3 x_j, float h)
{
    f3 grad;

    float delta = 1e-4f;
    f3 e1(1.0f, 0.0, 0.0);
    f3 e2(0.0f, 1.0, 0.0);
    f3 e3(0.0f, 0.0, 1.0);
    float W = this->W(x_i, x_j, h);

    float dx1 = this->W(x_i + e1.factor(delta), x_j, h);
    float dy1 = this->W(x_i + e2.factor(delta), x_j, h);
    float dz1 = this->W(x_i + e3.factor(delta), x_j, h);
    float dx2 = this->W(x_i - e1.factor(delta), x_j, h);
    float dy2 = this->W(x_i - e2.factor(delta), x_j, h);
    float dz2 = this->W(x_i - e3.factor(delta), x_j, h);

    grad.x = (dx1 - dx2) / (2.0f * delta);
    grad.y = (dy1 - dy2) / (2.0f * delta);
    grad.z = (dz1 - dz2) / (2.0f * delta);
    return grad;
}

//-----------------------------------------------------------------------------

float GaussKernel2D::W(f3 x_i, f3 x_j, float h)
{
    float h2 = h*h;
    float norm2 = (x_i - x_j).norm2();

    // Leave if neighbor is not supported
    if (norm2 > RADIUS2(h))
    { return 0.0f; }

    float q2 = norm2 / h2;
    float f_q = exp(-q2);
    return (1/(PI*h2) * f_q);
}