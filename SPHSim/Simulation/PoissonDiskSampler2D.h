
#ifndef SIMULATION_POISSONSAMPLER
#define SIMULATION_POISSONSAMPLER

#include <assert.h>
#include <list>
#include <random>
#include <vector>

#include "Particle.h"
#include "PatternShape.h"
#include "ServerGrid.h"

class PoissonDiskSampler2D
{
public:
    PoissonDiskSampler2D(const PatternShape& shape, float radius, unsigned tries = 30);

    void Sample(ParticleList* particles);
    
private:
    void add_position(f3& position);
    void init_grid();
    f3   to_coordinates(f3& position, float radius, float angle);
    bool valid(f3& position);

    std::vector<f3>     m_active;
    ServerGrid          m_grid;
    std::vector<f3>     m_positions;
    float               m_radius;
    const PatternShape& m_shape;
    unsigned            m_tries;

};

#endif