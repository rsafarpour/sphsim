
#ifndef SIMULATION_SIMULATION
#define SIMULATION_SIMULATION

#include <algorithm>
#include <memory>

#include "BoxContainer.h"
#include "BoxContainerGenerator2D.h"
#include "BoxGenerator2D.h"
#include "ClientGrid.h"
#include "GaussKernel2D.h"
#include "Properties.h"
#include "PoissonDiskSampler2D.h"
#include "SimulationWidget.h"
#include "ServerGrid.h"
#include "Grid.h"
#include "Grid.cuh"
#include "Equations.cuh"
#include "vmemory.cuh"

class Mesh;
class Simulation
{
public:
    Simulation();
    ~Simulation();

    Grid*         GetGrid();
    unsigned long GetNParticles();
    ParticleList& GetParticles();
    Widget*       GetWidget();
    bool          IsRunning();
    void          ResetFlags();
    void          Restart();
    void          Start();
    void          Stop();
    unsigned      Update(Mesh& particles, long ms_passed);
    bool          WasReconfigured();
    bool          WasUpdated();

private:
#ifndef GPU
    void      apply_particleupdates();
    void      calc_pressure(unsigned long i);
    void      calc_density(unsigned long i);
    void      calc_gravityforce(unsigned long i);
    void      calc_pressureforce(unsigned long i);
    void      calc_velocity_halfstep(unsigned long i);
    void      calc_velocities(unsigned long i);
    void      calc_viscosityforce(unsigned long i);
    IndexList gather_neighbors(unsigned long i);
    void      reset_forces();
#endif

    BoxContainer            m_container;
    Particle*               m_gpuparticles;
    std::unique_ptr<Grid>   m_grid;
    std::unique_ptr<Kernel> m_kernelfunc;
    float                   m_particle_mass;
    Properties              m_properties;
    ParticleList            m_particles;
    bool                    m_reconfigured;
    bool                    m_running;
    bool                    m_updated;
    SimulationWidget        m_widget;
};

#endif