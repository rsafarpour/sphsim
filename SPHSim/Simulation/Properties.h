
#ifndef SIMULATION_PROPERTIES
#define SIMULATION_PROPERTIES

struct Properties
{
    int      m_dt;   // Time step in ms
    float    m_g;    // Gravitation constant
    float    m_h;    // Kernel smoothing length
    float    m_k;    // Stiffness constant
    float    m_rho0; // Target density of resting fluid
    float    m_nu;   // Kinematic viscosity



    Properties() :
        m_dt(500),
        m_g(9.81f),
        m_h(1.0),
        m_k(1484.0f),    // <- Speed of sound in water
        m_rho0(998.0f),
        m_nu(10e-6f)
    {}
};

#endif