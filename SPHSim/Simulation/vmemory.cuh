
#ifndef SIMULATION_CUDA_VMEMORY
#define SIMULATION_CUDA_VMEMORY

#include <CudaDbg.h>

typedef void* gpu_addr;
typedef void* cpu_addr;

namespace cuda
{
    namespace vram
    {
        gpu_addr allocate(size_t bytes);
        void     free(gpu_addr addr);
        void     transfer_in(cpu_addr from, gpu_addr to, size_t bytes);
        void     transfer_out(gpu_addr from, cpu_addr to, size_t bytes);
    }
}

#endif