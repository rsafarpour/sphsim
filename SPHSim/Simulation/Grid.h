
#ifndef SIMULATION_GRID_GRID
#define SIMULATION_GRID_GRID

#include <f3.h>

#include "Cell.h"

class Grid
{
public:
    virtual void     Clear()                                          = 0;
    virtual void     Init(float dim, float min_cellsize)              = 0;
    virtual void     Insert(unsigned long i, f3& position)            = 0;
    virtual void     Update(unsigned long i, f3 from, f3 to)          = 0;
    virtual Cell*    GetCell(unsigned x, unsigned y, unsigned z)      = 0;
    virtual Cell*    GetCell(f3& position)                            = 0;
    virtual f3       GetMax()                                         = 0;
    virtual f3       GetMin()                                         = 0;
    virtual CellList GetNeighbors(unsigned x, unsigned y, unsigned z) = 0;
    virtual CellList GetNeighbors(f3& position)                       = 0;
};

#endif