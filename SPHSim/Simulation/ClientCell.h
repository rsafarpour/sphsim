
#ifndef SIMULATION_GRID_CLIENTCELL
#define SIMULATION_GRID_CLIENTCELL

#include <assert.h>

#include "Grid.cuh"
#include "Cell.h"

class ClientCell : public Cell
{
public:
    ClientCell(gpu_addr grid, gpu_addr c);

    virtual void      Add(unsigned long index)      override;
    virtual void      Clear()                       override;
    virtual bool      Contains(unsigned long index) override;
    virtual void      Remove(unsigned long index)   override;
    virtual IndexList GetIndices()                  override;

private:
    gpu_addr m_grid;
    gpu_addr m_cell;
};

#endif