
#ifndef SIMULATION_BOXSHAPE
#define SIMULATION_BOXSHAPE

#include "PatternShape.h"

class BoxShape : public PatternShape
{
    virtual float3 GetMin();
    virtual float3 GetMax();
    virtual bool IsInside(float3& point);
};

#endif