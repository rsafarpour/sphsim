
#include <Mesh.h>
#include "Simulation.h"

Simulation::Simulation()
  : m_container(f3(-8.0f, -8.0f, -8.0f), f3(8.0f, 8.0f, 8.0f)), // TODO: make this dynamic
    m_gpuparticles(nullptr),
    m_kernelfunc(new GaussKernel2D()),
    m_particle_mass(-1.0f),
    m_reconfigured(true),
    m_running(false),
    m_updated(true),
    m_widget(*this),
#ifndef GPU
    m_grid(new ServerGrid)
#else
    m_grid(new ClientGrid)
#endif
{
#ifdef GPU
    cudaDeviceSetLimit(cudaLimitStackSize, 10 * 1024);
#endif

    this->Restart();
    this->Stop();
}

//-----------------------------------------------------------------------------

Simulation::~Simulation()
{}

//-----------------------------------------------------------------------------

Grid* Simulation::GetGrid()
{
    return this->m_grid.get();
}

//-----------------------------------------------------------------------------

unsigned long Simulation::GetNParticles()
{
    return this->m_particles.size();
}

//-----------------------------------------------------------------------------

ParticleList& Simulation::GetParticles()
{
    return this->m_particles;
}

//-----------------------------------------------------------------------------

Widget* Simulation::GetWidget()
{
   return &this->m_widget;
}

//-----------------------------------------------------------------------------

bool Simulation::IsRunning()
{
    return this->m_running;
}

//-----------------------------------------------------------------------------

void Simulation::ResetFlags()
{
    this->m_reconfigured = false;
    this->m_updated = false;
}

//-----------------------------------------------------------------------------

void Simulation::Restart()
{
    this->Stop();
    this->m_grid->Clear();

#ifdef GPU
    if (this->m_gpuparticles != nullptr)
    { cuda::vram::free(this->m_gpuparticles); }
#endif

    this->m_properties = this->m_widget.m_props;


    // Hard coded for the time being
    this->m_particles.clear();
    BoxGenerator2D box(f3(-3, -3, -3), f3(3, 3, 3));
    box.GetParticles(this->m_properties.m_h, &this->m_particles);

    BoxContainerGenerator2D container(
        f3(-8, -8, -8),
        f3(8, 8, 8),
        f3(-10, -10, -10),
        f3(10, 10, 10));
    ParticleList container_particles;
    container.GetParticles(this->m_properties.m_h, &container_particles);
    this->m_particles.insert(
        this->m_particles.end(), 
        container_particles.begin(),
        container_particles.end());

    this->m_grid->Init(100.0f, RADIUS(this->m_properties.m_h));

    float h2 = this->m_properties.m_h * this->m_properties.m_h;
    this->m_particle_mass = this->m_properties.m_rho0 * h2;

    for (unsigned long i = 0;  i < this->m_particles.size(); i++)
    { this->m_grid->Insert(i, this->m_particles[i].m_position); }

// Allocate space on GPU to avoid constant host<->device copying
#ifdef GPU
    size_t bytes = sizeof(Particle) * this->m_particles.size();
    this->m_gpuparticles = static_cast<Particle*>(cuda::vram::allocate(bytes));

    cpu_addr src = this->m_particles.data();
    gpu_addr dst = this->m_gpuparticles;
    cuda::vram::transfer_in(src, dst, bytes);
#endif

    this->m_reconfigured = true;
    this->Start();

}

//-----------------------------------------------------------------------------

void Simulation::Start()
{
    this->m_running = true;
}

//-----------------------------------------------------------------------------

void Simulation::Stop()
{
    this->m_running = false;
}

//-----------------------------------------------------------------------------

unsigned Simulation::Update(Mesh& mesh, long ms_passed)
{
    if (!this->m_running)
    { return 0; }

    unsigned iter = 0, max_iter = 10;
    long ms_left = ms_passed;
    while (ms_left > this->m_properties.m_dt && iter < max_iter)
    {
        iter++;
#ifndef GPU
        reset_forces();

        for (unsigned long i = 0; i < this->m_particles.size(); i++)
        {
            calc_density(i);
            calc_pressure(i);
        }

        for (unsigned long i = 0; i < this->m_particles.size(); i++)
        {
            if (this->m_particles[i].m_type == ParticleType::FLUID)
            {
                calc_pressureforce(i);
                calc_viscosityforce(i);
                calc_gravityforce(i);
                calc_velocity_halfstep(i);
            }
        }

        for (unsigned long i = 0; i < this->m_particles.size(); i++)
        { 
            if (this->m_particles[i].m_type == ParticleType::FLUID)
            { calc_velocities(i); }
        }

        apply_particleupdates();

        ms_left -= this->m_properties.m_dt;
#else

        unsigned long n = this->m_particles.size();
        ClientGrid* grid = static_cast<ClientGrid*>(this->m_grid.get());
        cuda_grid* active_grid   = static_cast<cuda_grid*>(grid->GetActiveGrid());
        cuda_grid* inactive_grid = static_cast<cuda_grid*>(grid->GetInactiveGrid());
        float3* buffer = mesh.CudaMap();

        float dt = (float)this->m_properties.m_dt / 1000.0f;

        cuda::run::calc_densities(active_grid, this->m_particle_mass, this->m_properties.m_h, this->m_gpuparticles, n);
        cuda::run::calc_pressures(active_grid, this->m_particle_mass, this->m_properties.m_h, this->m_properties.m_rho0, this->m_properties.m_k, this->m_gpuparticles, n);
        cuda::run::calc_pressureforces(active_grid, this->m_particle_mass, this->m_properties.m_h, this->m_gpuparticles, n);
        cuda::run::calc_viscosityforces(active_grid, this->m_particle_mass, this->m_properties.m_h, this->m_properties.m_nu, this->m_gpuparticles, n);
        cuda::run::calc_gravityforces(this->m_properties.m_g, this->m_particle_mass, active_grid, this->m_gpuparticles, n);
        cuda::run::calc_velocity_halfstep(dt, this->m_particle_mass, active_grid, this->m_gpuparticles, n);
        cuda::run::calc_velocities(active_grid, this->m_particle_mass, this->m_properties.m_h, this->m_gpuparticles, n);
        cuda::run::apply_particleupdates(dt, this->m_gpuparticles, buffer, n);

        this->m_container.clip(this->m_gpuparticles, n);

        cuda::run::grid_update(inactive_grid, this->m_gpuparticles, n);

        mesh.CudaUnmap();

        grid->FlipGrids();

#endif
    }

    this->m_updated = true;
    return ms_left;
}

//-----------------------------------------------------------------------------

bool Simulation::WasReconfigured()
{
    return this->m_reconfigured;
}

//-----------------------------------------------------------------------------

bool Simulation::WasUpdated()
{
    return this->m_updated;
}

//-----------------------------------------------------------------------------
#ifndef GPU
void Simulation::apply_particleupdates()
{
    float dt = this->m_properties.m_dt / 1000.0f;

    // Move particels
    for (unsigned long i = 0; i < this->m_particles.size(); i++)
    {
        if (this->m_particles[i].m_type == ParticleType::FLUID)
        {
            Particle& p = this->m_particles[i];
            f3 old_position = p.m_position;
            f3 new_position = p.m_position + p.m_velocity.factor(dt);

            p.m_oldposition = p.m_position;
            p.m_position = new_position;
        }
    }

    // Clip particles against container
    this->m_container.clip(this->m_particles.data(), this->m_particles.size());

    // Update particles in grid
    for (unsigned long i = 0; i < this->m_particles.size(); i++)
    {
        if (this->m_particles[i].m_type == ParticleType::FLUID)
        {
            Particle& p = this->m_particles[i];
            this->m_grid->Update(i, p.m_oldposition, p.m_position);
        }
    }
}

//-----------------------------------------------------------------------------

void Simulation::calc_density(unsigned long i)
{
    IndexList indices = this->gather_neighbors(i);

    float rho_i = 0.0;
    for (unsigned long j : indices)
    {
        f3& x_i = m_particles[i].m_position;
        f3& x_j = m_particles[j].m_position;

        float w = this->m_kernelfunc->W(x_i, x_j, this->m_properties.m_h);
        rho_i += w * this->m_particle_mass;
    }

    this->m_particles[i].m_density = rho_i;
}

//-----------------------------------------------------------------------------

void Simulation::calc_gravityforce(unsigned long i)
{
    f3 F_g(0.0f, -1.0f, 0.0f);
    F_g = F_g.factor(this->m_particle_mass * this->m_properties.m_g);

    this->m_particles[i].m_force += F_g;
}

//-----------------------------------------------------------------------------

void Simulation::calc_pressure(unsigned long i)
{
    Particle* p = &this->m_particles[i];
    float p_i = this->m_particles[i].m_density / this->m_properties.m_rho0;
    p_i = pow(p_i, 7.0f) - 1.0f;

    p->m_pressure = this->m_properties.m_k * p_i;
}

//-----------------------------------------------------------------------------

void Simulation::calc_pressureforce(unsigned long i)
{
    Particle& particle_i = this->m_particles[i];
    f3& x_i = particle_i.m_position;
    float rho_i = particle_i.m_density;
    float p_i = particle_i.m_pressure;
    float m_i = this->m_particle_mass;

    // Calculate pressure gradient at x_i
    f3 p_sum;
    IndexList indices = this->gather_neighbors(i);
    for (unsigned long j : indices)
    {
        //if (j == i) { continue; }

        Particle& particle_j = this->m_particles[j];
        float rho_j   = particle_j.m_density;
        float p_j     = particle_j.m_pressure;
        float m_j     = m_i;
        f3& x_j   = particle_j.m_position;
        f3& x_ij  = x_i - x_j;
        
        f3 gradW = this->m_kernelfunc->gradW(x_i, x_j, this->m_properties.m_h);
        
        // Gradient of p field
        float f = (p_i / (rho_i*rho_i)) + (p_j / (rho_j * rho_j));
        p_sum += gradW.factor(m_j * f);

    }

    f3 F_pressure = p_sum.factor(-m_i);
    particle_i.m_force += F_pressure;
}

//-----------------------------------------------------------------------------

void Simulation::calc_velocity_halfstep(unsigned long i)
{
    float dt = this->m_properties.m_dt / 1000.0f;
    Particle& p_i = this->m_particles[i];

    p_i.m_velocity += p_i.m_force.factor(dt / this->m_particle_mass);
}

//-----------------------------------------------------------------------------

void Simulation::calc_velocities(unsigned long i)
{
    static const float epsilon = 0.05f;
    Particle& p_i = this->m_particles[i];
    f3&       v_i = p_i.m_velocity;
    f3&       x_i = p_i.m_position;
    float&    rho_i = p_i.m_density;

    IndexList indices = this->gather_neighbors(i);
    f3 sum;
    for (unsigned long j : indices)
    {
        if (j == i) { continue; }

        Particle& p_j = this->m_particles[j];
        float&    rho_j = p_j.m_density;
        f3&       v_j = p_j.m_velocity;
        f3&       x_j = p_j.m_position;
        f3        v_ij = v_j - v_i;
        float     m_j = this->m_particle_mass;

        float W = this->m_kernelfunc->W(x_i, x_j, this->m_properties.m_h);
        sum += v_ij.factor((m_j*W) / rho_j);
    }

    p_i.m_velocity = p_i.m_velocity + sum.factor(epsilon);
}

//-----------------------------------------------------------------------------

void Simulation::calc_viscosityforce(unsigned long i)
{
    Particle* particle_i = &this->m_particles[i];
    f3 x_i = particle_i->m_position;

    f3 v_laplacian;
    IndexList indices = this->gather_neighbors(i);
    for (unsigned long j : indices)
    {
        if (j == i) { continue; }

        Particle* particle_j = &this->m_particles[j];
        f3 x_j = particle_j->m_position;
        f3 x_ij = x_i - x_j;
        f3 v_ij = particle_i->m_velocity - particle_j->m_velocity;
        f3 grad = this->m_kernelfunc->gradW(x_i, x_j, this->m_properties.m_h);
        float m_j = this->m_particle_mass;
        float rho_j = particle_j->m_density;

        float h2 = this->m_properties.m_h * this->m_properties.m_h;
        float frac = grad.dot(x_ij) / (x_ij.dot(x_ij) + 0.01f * h2);

        // Laplacian of v field
        v_laplacian += v_ij.factor(m_j / rho_j * frac);
    }

    v_laplacian = v_laplacian.factor(2);

    f3 F_visc = v_laplacian.factor(this->m_particle_mass * this->m_properties.m_nu);
    particle_i->m_force += F_visc;
}

//-----------------------------------------------------------------------------

IndexList Simulation::gather_neighbors(unsigned long i)
{
    CellList cells = this->m_grid->GetNeighbors(this->m_particles[i].m_position);

    // Neighbor cells
    IndexList indices;
    for (Cell* cell : cells)
    {
        IndexList new_indices = cell->GetIndices();
        indices.insert(indices.end(), new_indices.begin(), new_indices.end());
    }
    return indices;
}

//-----------------------------------------------------------------------------

void Simulation::reset_forces()
{
    for (unsigned long i = 0; i < this->m_particles.size(); i++)
    { this->m_particles[i].m_force = f3(); }
}

#endif