
#ifndef SIMLUATION_GRID_CLIENTGRID
#define SIMULATION_GRID_CLIENTGRID

#include <Dbg.h>

#include "ClientCell.h"
#include "Grid.cuh"
#include "Grid.h"

#define N_NEIGHBORS 27

class ClientGrid : public Grid
{
public:
    ClientGrid();
    ~ClientGrid();

    virtual void     Clear()                                          override;
    virtual void     Init(float dim, float min_cellsize)              override;
    virtual void     Insert(unsigned long i, f3& position)            override;
    virtual void     Update(unsigned long i, f3 from, f3 to)          override;
    virtual Cell*    GetCell(unsigned x, unsigned y, unsigned z)      override;
    virtual Cell*    GetCell(f3& position)                            override;
    virtual f3       GetMax()                                         override;
    virtual f3       GetMin()                                         override;
    virtual CellList GetNeighbors(unsigned x, unsigned y, unsigned z) override;
    virtual CellList GetNeighbors(f3& position)                       override;
    void     FlipGrids();
    gpu_addr GetActiveGrid();
    gpu_addr GetInactiveGrid();

private:
    void indices_from_position(
        f3& position,
        unsigned* x,
        unsigned* y,
        unsigned* z);

    long         m_dim_cells;
    gpu_addr     m_d_activegrid;
    gpu_addr     m_d_grid1;
    gpu_addr     m_d_grid2;
    ClientCell** m_h_cells;
};

#endif