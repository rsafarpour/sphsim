
#ifndef SIMULATION_BOXGENERATOR
#define SIMULATION_BOXGENERATOR

#include <assert.h>
#include <vector>

#include "ParticleGenerator.h"
#include "PatternShape.h"
#include "PoissonDiskSampler2D.h"

#define SAMPLING_E 1.085f

class BoxGenerator2D 
  : public ParticleGenerator,
    public PatternShape
{
public:
    BoxGenerator2D(f3 min, f3 max);

    virtual f3   GetMin()                                               const override;
    virtual f3   GetMax()                                               const override;
    virtual void GetParticles(float h, ParticleList* particles)         const override;
            void GetSurfaceParticles(ParticleList* particles, float h)  const;
    virtual bool IsInside(f3& point)                                    const override;
    virtual void SetDomain(f3 min, f3 max)                                    override;

private:
    float     m_kernelsupport;
    f3        m_min;
    f3        m_max;
};

#endif