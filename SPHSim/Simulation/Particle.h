
#ifndef SIMULATION_PARTICLE
#define SIMULATION_PARTICLE

#include <cuda_runtime.h>
#include <f3.h>
#include <vector>

enum class ParticleType
{
    FLUID,
    SOLID
};

struct Particle
{
    ParticleType m_type;
    float        m_density;
    float        m_pressure;
    f3           m_force;
    f3           m_position;
    f3           m_oldposition;
    f3           m_velocity;

    __host__ __device__ Particle() :
        m_type(ParticleType::FLUID),
        m_density(0.0f),
        m_pressure(0.0f)
    {};

};

typedef std::vector<Particle> ParticleList;

#endif