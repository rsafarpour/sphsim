
#ifndef SIMULATION_CUDA_EQUATIONS
#define SIMULATION_CUDA_EQUATIONS

#include <cuda.h>
#include <cuda_runtime.h>

#include "float3.cuh"
#include "cudaconf.h"
#include "GaussKernel2D.h"
#include "CudaDbg.h"
#include "Grid.cuh"
#include "Particle.h"

namespace cuda
{
    namespace device
    {
        __device__ float  gauss_W(float3 x_i, float3 x_j, float h);
        __device__ float3 gauss_gradW(float3 x_i, float3 x_j, float h);
    }

    namespace kernel
    {
        __global__ void calc_densities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        __global__ void calc_pressures(cuda_grid* grid, float mass, float h, float rho0, float k, Particle* particles, unsigned long n_particles);
        __global__ void calc_pressureforces(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        __global__ void calc_viscosityforces(unsigned long n_particles, cuda_grid* grid, float mass, float h, float nu, Particle* particles);
        __global__ void calc_gravityforces(float G, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles);
        __global__ void calc_velocity_halfstep(float dt, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles);
        __global__ void calc_velocities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        __global__ void apply_particleupdates(float dt, Particle* particles, float3* positions, unsigned long n_particles);

        __global__ void grid_update(cuda_grid* inactive_grid, Particle* particles, unsigned long n_particles);
    }

    namespace run
    {
        void calc_densities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        void calc_pressures(cuda_grid* grid, float mass, float h, float rho0, float k, Particle* particles, unsigned long n_particles);
        void calc_pressureforces(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        void calc_viscosityforces(cuda_grid* grid, float mass, float h, float nu, Particle* particles, unsigned long n_particles);
        void calc_gravityforces(float G, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles);
        void calc_velocity_halfstep(float dt, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles);
        void calc_velocities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles);
        void apply_particleupdates(float dt, Particle* particles, float3* positions, unsigned long n_particles);
        void grid_update(cuda_grid* inactive_grid, Particle* particles, unsigned long n_particles);
    }
}
#endif