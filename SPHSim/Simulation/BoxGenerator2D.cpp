
#include "BoxGenerator2D.h"

BoxGenerator2D::BoxGenerator2D(f3 min, f3 max)
  : m_min(min),
    m_max(max)
{
    assert(min.x < max.x);
    assert(min.y < max.y);
    assert(min.z < max.z);
}

//-----------------------------------------------------------------------------

f3 BoxGenerator2D::GetMin() const
{
    return this->m_min;
}

//-----------------------------------------------------------------------------

f3 BoxGenerator2D::GetMax() const
{
    return this->m_max;
}

//-----------------------------------------------------------------------------

void BoxGenerator2D::GetParticles(float h, ParticleList* particles) const
{
    this->GetSurfaceParticles(particles, h);

    PoissonDiskSampler2D sampler(*this, SAMPLE_RADIUS(h));
    sampler.Sample(particles);

    // Tag all particles as fluid particles
    for (Particle& p : *particles)
    { p.m_type = ParticleType::FLUID; }
}

//-----------------------------------------------------------------------------

void BoxGenerator2D::GetSurfaceParticles(ParticleList* particles, float h) const
{
    f3 edges = this->m_max - this->m_min;
    float point_distance = SAMPLING_E * SAMPLE_RADIUS(h);

    // Bottom of Box:
    float left = edges.x;
    float walked = 0.0f;
    while (left - point_distance > 0.0)
    {
        Particle p;
        p.m_position = f3(this->m_min.x + walked, this->m_min.y, 0.0f);
        particles->push_back(p);

        walked += point_distance;
        left -= point_distance;
    }

    // Right of Box:
    walked = left;
    left = edges.y - walked;
    while (left - point_distance > 0.0)
    {
        Particle p;
        p.m_position = f3(this->m_max.x, this->m_min.y + walked, 0.0f);
        particles->push_back(p);

        walked += point_distance;
        left -= point_distance;
    }

    // Top of Box:
    walked = left;
    left = edges.y - walked;
    while (left - point_distance > 0.0)
    {
        Particle p;
        p.m_position = f3(this->m_max.x - walked, this->m_max.y, 0.0f);
        particles->push_back(p);

        walked += point_distance;
        left -= point_distance;
    }

    // Left of Box:
    walked = left;
    left = edges.y - walked;
    while (left - point_distance > 0.0)
    {
        Particle p;
        p.m_position = f3(this->m_min.x, this->m_max.y - walked, 0.0f);
        particles->push_back(p);

        walked += point_distance;
        left -= point_distance;
    }
}

//-----------------------------------------------------------------------------

void BoxGenerator2D::SetDomain(f3 min, f3 max)
{
    assert(min.x < max.x);
    assert(min.y < max.y);
    assert(min.z < max.z);

    if (this->m_min.x < min.x)
    { this->m_min.x = min.x; }
    if (this->m_min.y < min.y)
    { this->m_min.y = min.y; }
    if (this->m_min.z < min.z)
    { this->m_min.z = min.z; }

    if (this->m_max.x > max.x)
    { this->m_max.x = max.x; }
    if (this->m_max.y > max.y)
    { this->m_max.y = max.y; }
    if (this->m_max.z > max.z)
    { this->m_max.z = max.z; }
}

//-----------------------------------------------------------------------------

bool BoxGenerator2D::IsInside(f3& point) const
{
    if (point.x < this->m_min.x ||
        point.y < this->m_min.y ||
        point.z < this->m_min.z || 
        point.x > this->m_max.x ||
        point.y > this->m_max.y ||
        point.z > this->m_max.z)
    { return false; }
    else
    { return true; }
}