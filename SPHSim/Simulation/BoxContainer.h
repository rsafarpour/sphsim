
#ifndef SIMULATION_BOXCONTAINER
#define SIMULATION_BOXCONTAINER

#define N_BOX_PLANES 6

#include <Plane.h>

#include "Container.h"
#include "cudaconf.h"
#include "CudaDbg.h"
#include "vmemory.cuh"

class BoxContainer : public Container
{
public:
    BoxContainer(f3 min, f3 max);
    ~BoxContainer();

    virtual void clip(Particle* particles, unsigned long n_particles) override;

private:

    Plane* m_gpuplanes;
    Plane m_planes[N_BOX_PLANES];
};


#endif