#include "BoxContainer.cuh"
#include "BoxContainer.h"

namespace cuda
{

    __global__ void kernel::clip(Plane* planes, Particle* particles, unsigned long n_particles)
    {
        unsigned index = THREAD_INDEX;

        if (index >= n_particles) { return; }

        if (particles[index].m_type == ParticleType::FLUID)
        {
            for (unsigned i = 0; i < N_BOX_PLANES; i++)
            { projection(&planes[i], &particles[index].m_position); }
        }
    }

    //=============================================================================

    void run::clip(Plane* planes, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::clip << < blocks, N_THREADS >> >(planes, particles, n_particles);
        check(cudaGetLastError());
    }


//=============================================================================

    __host__ __device__ void projection(Plane* plane, f3* position)
    {

        float3 normal;
        normal.x = plane->m_coefficients.x;
        normal.y = plane->m_coefficients.y;
        normal.z = plane->m_coefficients.z;

        float distance = 0;
        float3 point = to_float3(*position);
        distance += plane->m_coefficients.x * point.x;
        distance += plane->m_coefficients.y * point.y;
        distance += plane->m_coefficients.z * point.z;
        distance += plane->m_coefficients.w;

        if (distance > 0.0)
        {
            float3 new_position = point - factor(normal, distance);
            position->x = new_position.x;
            position->y = new_position.y;
            position->z = new_position.z;
        }
    }
}