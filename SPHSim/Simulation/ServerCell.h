
#ifndef SIMULATION_GRID_SERVERCELL
#define SIMULATION_GRID_SERVERCELL

#include <assert.h>
#include <Dbg.h>
#include <vector>

#include "Cell.h"

class ServerCell : public Cell
{
public:
    virtual void Add(unsigned long index)      override;
    virtual void Clear()                       override;
    virtual bool Contains(unsigned long index) override;
    virtual void Remove(unsigned long index)   override;
    virtual IndexList GetIndices()             override;

private:
    IndexList m_indices;
};

#endif