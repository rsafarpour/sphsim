
#ifndef SIMULATION_KERNEL
#define SIMULATION_KERNEL

#include <f3.h>

class Kernel
{
public:
    virtual f3 gradW(f3 x_i, f3 x_j, float h) = 0;
    virtual float      W(f3 x_i, f3 x_j, float h) = 0;
};

#endif