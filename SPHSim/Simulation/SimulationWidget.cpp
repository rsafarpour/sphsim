#include "SimulationWidget.h"
#include "Simulation.h"

SimulationWidget::SimulationWidget(Simulation& sim)
  : m_sim(sim)
{}

//-----------------------------------------------------------------------------

void SimulationWidget::Definition()
{
    ImGui::SetNextWindowPos(ImVec2(5.0, 5.0), ImGuiSetCond_Once);
    ImGui::SetNextWindowSize(ImVec2(155, 200), ImGuiSetCond_Always);
    ImGui::Begin("Simulation", nullptr, 
        ImGuiWindowFlags_NoResize | 
        ImGuiWindowFlags_NoMove);

    // Edit fields:
    ImGui::InputInt("dt", &this->m_props.m_dt, 1, 5);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Timestep im ms");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat("G",    &this->m_props.m_g, 0.5, 1.0, 2);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Gravitational constant in m/s^2");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat("h", &this->m_props.m_h, 0.1f, 0.5f, 2);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Smoothing length");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat("k",    &this->m_props.m_k, 0.1f, 0.5f, 2);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Stiffness constant\n for viscosity");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat("rho0", &this->m_props.m_rho0, 1.0, 0.5, 0);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Rest density in kg/m^3");
        ImGui::EndTooltip();
    }

    ImGui::InputFloat("nu",   &this->m_props.m_nu, 0.0001f, 0.1f, 5);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Kinematic viscosity in m^2/s");
        ImGui::EndTooltip();
    }

    // Buttons:
    if (this->m_sim.IsRunning())
    {
        if (ImGui::Button("Pause"))
            this->m_sim.Stop();
    }
    else
    {
        if(ImGui::Button(" Run "))
            this->m_sim.Start();
    }
    ImGui::SameLine();

    if (ImGui::Button("Restart"))
        this->m_sim.Restart();


    ImGui::End();
}