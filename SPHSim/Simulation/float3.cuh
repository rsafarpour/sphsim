
#ifndef SIMULATION_CUDA_FLOAT3
#define SIMULATION_CUDA_FLOAT3

#include <cuda.h>
#include <cuda_runtime.h>
#include <f3.h>


__host__ __device__ void operator+=(float3& a, const float3& b);
__host__ __device__ float3 operator+(const float3& a, const float3& b);
__host__ __device__ float3 operator-(const float3& a, const float3& b);
__host__ __device__ void operator-=(float3& a, const float3& b);
__host__ __device__ float3 factor(float3& a, const float& b);
__host__ __device__ float dot(const float3& a, const float3& b);
__host__ __device__ float3 to_float3(const f3& a);


#endif