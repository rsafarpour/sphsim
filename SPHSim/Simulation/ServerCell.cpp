
#include "ServerCell.h"

void ServerCell::Add(unsigned long index)
{
    this->m_indices.push_back(index);
}

//-----------------------------------------------------------------------------

void ServerCell::Clear()
{
    this->m_indices.clear();
}

//-----------------------------------------------------------------------------

bool ServerCell::Contains(unsigned long index)
{
    auto i = std::find(this->m_indices.begin(), this->m_indices.end(), index);
    return (i != this->m_indices.end());
}

//-----------------------------------------------------------------------------

void ServerCell::Remove(unsigned long index)
{
    auto i = std::find(this->m_indices.begin(), this->m_indices.end(), index);

    assert(i != this->m_indices.end());
    this->m_indices.erase(i);
}

//-----------------------------------------------------------------------------

IndexList ServerCell::GetIndices()
{
    return this->m_indices;
}