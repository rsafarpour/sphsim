
#include "ServerGrid.h"

ServerGrid::ServerGrid()
  : m_cells(nullptr),
    m_cellsize(-1.0f)
{}

//-----------------------------------------------------------------------------

ServerGrid::~ServerGrid()
{
    if (this->m_cells)
    { delete[] this->m_cells; }
}

//-----------------------------------------------------------------------------

void ServerGrid::Clear()
{
    for (unsigned i = 0; i < this->m_n_cells; i++)
    { this->m_cells[i].Clear(); }
}

//-----------------------------------------------------------------------------

void ServerGrid::Init(float dim, float min_cellsize)
{
    // Force even number of cells in dimension
    unsigned n_dim_cells = static_cast<unsigned>(ceil(dim / min_cellsize));
    n_dim_cells -= n_dim_cells % 2;

    this->m_n_cells = n_dim_cells * n_dim_cells * n_dim_cells;
    this->m_cells = new ServerCell[this->m_n_cells];

    this->m_cellsize = dim / (n_dim_cells-1);
    this->m_dim_cells = n_dim_cells;
}

//-----------------------------------------------------------------------------

void ServerGrid::Insert(unsigned long i, f3& position)
{
    Cell* cell = this->GetCell(position);
    cell->Add(i);
}

//-----------------------------------------------------------------------------

void ServerGrid::Update(unsigned long i, f3 from, f3 to)
{
    // Check if to is in cell
    unsigned i_x_old, i_y_old, i_z_old;
    unsigned i_x_new, i_y_new, i_z_new;
    this->indices_from_position(from, &i_x_old, &i_y_old, &i_z_old);
    this->indices_from_position(to, &i_x_new, &i_y_new, &i_z_new);

    if (i_x_old != i_x_new || 
        i_y_old != i_y_new || 
        i_z_old != i_z_new)
    {
        // Update cell
        ServerCell* old_cell = static_cast<ServerCell*>(this->GetCell(i_x_old, i_y_old, i_z_old));
        ServerCell* new_cell = static_cast<ServerCell*>(this->GetCell(i_x_new, i_y_new, i_z_new));

        old_cell->Remove(i);
        new_cell->Add(i);
    }

}

//-----------------------------------------------------------------------------

Cell* ServerGrid::GetCell(unsigned x, unsigned y, unsigned z)
{
    assert(x < this->m_dim_cells);
    assert(y < this->m_dim_cells);
    assert(z < this->m_dim_cells);

    unsigned i = x;
    i += this->m_dim_cells * y;
    i += this->m_dim_cells * this->m_dim_cells * z;

    assert(i < this->m_n_cells);

    return &this->m_cells[i];
}

//-----------------------------------------------------------------------------


f3 ServerGrid::GetMax()
{
    float half_dim = (this->m_dim_cells * this->m_cellsize)/2.0f;
    f3 half_extent = f3(half_dim, half_dim, half_dim);

    return (f3::O + half_extent);
}

//-----------------------------------------------------------------------------

f3 ServerGrid::GetMin()
{
    float half_dim = (this->m_dim_cells * this->m_cellsize) / 2.0f;
    f3 half_extent = f3(half_dim, half_dim, half_dim);

    return (f3::O - half_extent);
}

//-----------------------------------------------------------------------------

Cell* ServerGrid::GetCell(f3& position)
{
    unsigned i_x, i_y, i_z;

    this->indices_from_position(position, &i_x, &i_y, &i_z);
    return this->GetCell(i_x, i_y, i_z);
}

//-----------------------------------------------------------------------------

CellList ServerGrid::GetNeighbors(unsigned x, unsigned y, unsigned z)
{
    CellList neighbors;
    unsigned x_min = max((int)x - 1, 0);
    unsigned x_max = min((int)x + 1, (int)this->m_dim_cells - 1);
    unsigned y_min = max((int)y - 1, 0);
    unsigned y_max = min((int)y + 1, (int)this->m_dim_cells - 1);
    unsigned z_min = max((int)z - 1, 0);
    unsigned z_max = min((int)z + 1, (int)this->m_dim_cells - 1);

    for (unsigned i = x_min; i <= x_max; i++)
    {
        for (unsigned j = y_min; j <= y_max; j++)
        {
            for (unsigned k = z_min; k <= z_max; k++)
            { 
                neighbors.push_back(this->GetCell(i, j, k));
            }
        }
    }
    return neighbors;
}

//-----------------------------------------------------------------------------

CellList ServerGrid::GetNeighbors(f3& position)
{
    unsigned i_x, i_y, i_z;

    this->indices_from_position(position, &i_x, &i_y, &i_z);
    return this->GetNeighbors(i_x, i_y, i_z);
}

//-----------------------------------------------------------------------------

void ServerGrid::indices_from_position(f3& position, unsigned* x,
    unsigned* y, unsigned* z)
{
    float half_dim = (this->m_dim_cells * this->m_cellsize) / 2;
    if (!(position.x >= -half_dim && position.x <= half_dim)) 
    {
        int i = 0;
        i++;
    }
    if (!(position.y >= -half_dim && position.y <= half_dim))
    {
        int i = 0;
        i++;
    }
    if (!(position.z >= -half_dim && position.z <= half_dim)) 
    {
        int i = 0;
        i++;
    }


    assert(position.x >= -half_dim && position.x <= half_dim);
    assert(position.y >= -half_dim && position.y <= half_dim);
    assert(position.z >= -half_dim && position.z <= half_dim);

    *x = static_cast<unsigned>((position.x + half_dim) / this->m_cellsize);
    *y = static_cast<unsigned>((position.y + half_dim) / this->m_cellsize);
    *z = static_cast<unsigned>((position.z + half_dim) / this->m_cellsize);
}