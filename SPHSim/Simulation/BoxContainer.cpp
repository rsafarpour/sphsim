
#include "BoxContainer.h"
#include "BoxContainer.cuh"

BoxContainer::BoxContainer(f3 min, f3 max)
{
    this->m_planes[0] = Plane(f3(-1.0,  0.0,  0.0), min);
    this->m_planes[1] = Plane(f3( 0.0, -1.0,  0.0), min);
    this->m_planes[2] = Plane(f3( 0.0,  0.0, -1.0), min);
    this->m_planes[3] = Plane(f3( 1.0,  0.0,  0.0), max);
    this->m_planes[4] = Plane(f3( 0.0,  1.0,  0.0), max);
    this->m_planes[5] = Plane(f3( 0.0,  0.0,  1.0), max);

#ifdef GPU

    cpu_addr src = this->m_planes;
    gpu_addr dest = cuda::vram::allocate(sizeof(Plane) * 6);
    cuda::vram::transfer_in(src, dest, sizeof(Plane) * 6);

    this->m_gpuplanes = static_cast<Plane*>(dest);

#endif
}

//-----------------------------------------------------------------------------

BoxContainer::~BoxContainer()
{
    cuda::vram::free(this->m_gpuplanes);
}

//-----------------------------------------------------------------------------

void BoxContainer::clip(Particle* particles, unsigned long n_particles)
{

#ifdef GPU

    cuda::run::clip(this->m_gpuplanes, particles, n_particles);

#else
    for (unsigned long i = 0; i < n_particles; i++)
    {
        for (unsigned j = 0; j < N_BOX_PLANES; j++)
        {
            Particle* p = &particles[i];
            if (p->m_type == ParticleType::FLUID && this->m_planes[j].distance(p->m_position) > 0.0f)
            {
                p->m_position = this->m_planes[j].projection(p->m_position);
            }
        }
    }
#endif
}