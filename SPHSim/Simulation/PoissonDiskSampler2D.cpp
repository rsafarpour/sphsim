
#include "PoissonDiskSampler2D.h"

// Poisson Disk Sampling as in http://www.cs.ubc.ca/~rbridson/docs/bridson-siggraph07-poissondisk.pdf

PoissonDiskSampler2D::PoissonDiskSampler2D(const PatternShape& shape, float radius, unsigned tries)
  : m_radius(radius),
    m_shape(shape),
    m_tries(tries)
{
    assert(radius > 0.0f);
    assert(tries > 0);
}

//-----------------------------------------------------------------------------

void PoissonDiskSampler2D::Sample(ParticleList* particles)
{
    this->init_grid();

    //Initialize random number generators
    std::random_device                    seed;
    std::mt19937                          gen(seed());
    std::uniform_real_distribution<float> r_dist(this->m_radius, 2 * this->m_radius);
    std::uniform_real_distribution<float> phi_dist(0.0f, 360.0f);

    for (Particle& p : *particles)
    { this->add_position(p.m_position); }

    for (unsigned long i = 0; this->m_active.size() > 0; i++)
    {
        // Take an active point then find and test candidates
        f3& current  = this->m_active.back();
        bool found_point = false;
        for (unsigned j = 0; j < this->m_tries && !found_point; j++)
        {
            f3 candidate = this->to_coordinates(current, r_dist(gen), phi_dist(gen));
            if (this->valid(candidate))
            {
                found_point = true;
                this->add_position(candidate);

                Particle p;
                p.m_oldposition = candidate;
                p.m_position = candidate;
                particles->push_back(p);
            }
        }

        if (!found_point)
        { this->m_active.pop_back(); }
    }
}

//-----------------------------------------------------------------------------

void PoissonDiskSampler2D::add_position(f3& position)
{
    this->m_grid.Insert(this->m_positions.size(), position);

    this->m_positions.push_back(position);
    this->m_active.push_back(position);
}

//-----------------------------------------------------------------------------

void PoissonDiskSampler2D::init_grid()
{
    f3 dims = this->m_shape.GetMax() - this->m_shape.GetMin();
    float dim = max(dims.x, max(dims.y, dims.z));

    this->m_grid.Init(dim, this->m_radius / sqrt((float)2));
}

//-----------------------------------------------------------------------------

f3 PoissonDiskSampler2D::to_coordinates(f3& position, float radius, float angle)
{
    float x = radius * cos(angle);
    float y = radius * sin(angle);

    return f3(position.x + x, position.y + y, 0.0);
}

//-----------------------------------------------------------------------------

bool PoissonDiskSampler2D::valid(f3& position)
{
    // Make sure the new position is inside shape
    if (!this->m_shape.IsInside(position))
    { return false; }

    CellList cells = this->m_grid.GetNeighbors(position);

    float radius2 = this->m_radius * this->m_radius;
    for (auto& cell : cells)
    {
        for (auto index : cell->GetIndices())
        {
            if ((this->m_positions[index] - position).norm2() < radius2)
            { return false; }
        }
    }

    return true;
}