#include "ClientGrid.h"

ClientGrid::ClientGrid()
  : m_d_activegrid(nullptr),
    m_h_cells(nullptr)
{}

//-----------------------------------------------------------------------------

ClientGrid::~ClientGrid()
{}

//-----------------------------------------------------------------------------

void ClientGrid::Clear()
{
    if (this->m_d_activegrid != nullptr)
    { cuda::host::ClearGrid(this->m_d_grid1, this->m_dim_cells); }

    if (this->m_d_activegrid != nullptr)
    { cuda::host::ClearGrid(this->m_d_grid2, this->m_dim_cells); }
}

//-----------------------------------------------------------------------------

void ClientGrid::Init(float dim, float min_cellsize)
{
    if (this->m_d_grid1) // Destroy any old grids if they exist
    {
        cuda::host::DestroyGrid(this->m_d_grid1);
        cuda::host::DestroyGrid(this->m_d_grid2);
    }

    cuda::host::CreateGrid(dim, min_cellsize, &this->m_d_grid1);
    cuda::host::CreateGrid(dim, min_cellsize, &this->m_d_grid2);
    this->m_d_activegrid = this->m_d_grid1;

    cuda_grid cpu_tmp;
    cuda::host::GetGrid(this->m_d_grid1, (cpu_addr*)&cpu_tmp);
    this->m_dim_cells = cpu_tmp.m_dim_cells;

    long n_cells = 
        this->m_dim_cells * 
        this->m_dim_cells * 
        this->m_dim_cells;
    this->m_h_cells = new ClientCell*[n_cells];

    for (long i = 0; i < n_cells; i++)
    { this->m_h_cells[i] = nullptr; }
}

//-----------------------------------------------------------------------------

void ClientGrid::Insert(unsigned long i, f3& position)
{
    if (this->m_d_activegrid)
    { cuda::host::InsertGrid(this->m_d_activegrid, i, position); }
}

//-----------------------------------------------------------------------------

void ClientGrid::Update(unsigned long i, f3 from, f3 to)
{
    dbg::error("CUDA Grid should not be updated but cleared/repopulated");
    return;
}

//-----------------------------------------------------------------------------

Cell* ClientGrid::GetCell(unsigned x, unsigned y, unsigned z)
{ 
    if (this->m_d_activegrid)
    {
        cuda_cell* cell;
        cuda::host::GetCell(this->m_d_activegrid, make_uint3(x, y, z), &cell);

        unsigned i = x;
        i += this->m_dim_cells * y;
        i += this->m_dim_cells * this->m_dim_cells * z;

        if (this->m_h_cells[i] != nullptr)
        { delete this->m_h_cells; }

        this->m_h_cells[i] = new ClientCell(this->m_d_activegrid, cell);

        return this->m_h_cells[i];
    }
    else
    { 
        return nullptr; 
    }
}

//-----------------------------------------------------------------------------

Cell* ClientGrid::GetCell(f3& position)
{
    unsigned i_x, i_y, i_z;

    this->indices_from_position(position, &i_x, &i_y, &i_z);
    return this->GetCell(i_x, i_y, i_z);
}

//-----------------------------------------------------------------------------

f3 ClientGrid::GetMax()
{
    if (this->m_d_activegrid)
    { 
        cuda_grid cpu_tmp;
        cuda::host::GetGrid(this->m_d_activegrid, (cpu_addr*)&cpu_tmp);
        return cpu_tmp.m_max;
    }
    else
    { return f3(); }
}

//-----------------------------------------------------------------------------

f3 ClientGrid::GetMin()
{
    if (this->m_d_activegrid)
    {
        cuda_grid cpu_tmp;
        cuda::host::GetGrid(this->m_d_activegrid, (cpu_addr*)&cpu_tmp);
        return cpu_tmp.m_min;
    }
    else
    { return f3(); }
}

//-----------------------------------------------------------------------------

CellList ClientGrid::GetNeighbors(unsigned x, unsigned y, unsigned z)
{ 
    if (this->m_d_activegrid)
    {
        CellList neighbors;
        unsigned x_min = max((int)x - 1, 0);
        unsigned x_max = min((int)x + 1, (int)this->m_dim_cells - 1);
        unsigned y_min = max((int)y - 1, 0);
        unsigned y_max = min((int)y + 1, (int)this->m_dim_cells - 1);
        unsigned z_min = max((int)z - 1, 0);
        unsigned z_max = min((int)z + 1, (int)this->m_dim_cells - 1);

        for (unsigned i = x_min; i <= x_max; i++)
        {
            for (unsigned j = y_min; j <= y_max; j++)
            {
                for (unsigned k = z_min; k <= z_max; k++)
                {
                    neighbors.push_back(this->GetCell(i, j, k));
                }
            }
        }
        return neighbors;
    }
    else
    { 
        return CellList(); 
    }
}

//-----------------------------------------------------------------------------

void ClientGrid::FlipGrids()
{
    if (this->m_d_activegrid == this->m_d_grid1)
    {
        this->m_d_activegrid = this->m_d_grid2;
        cuda::host::ClearGrid(this->m_d_grid1, this->m_dim_cells);
    }
    else
    {
        this->m_d_activegrid = this->m_d_grid1;
        cuda::host::ClearGrid(this->m_d_grid2, this->m_dim_cells);
    }
}

//-----------------------------------------------------------------------------

CellList ClientGrid::GetNeighbors(f3& position)
{ 
    unsigned i_x, i_y, i_z;
    this->indices_from_position(position, &i_x, &i_y, &i_z);
    return this->GetNeighbors(i_x, i_y, i_z);
}

//-----------------------------------------------------------------------------

gpu_addr ClientGrid::GetActiveGrid()
{
    return this->m_d_activegrid;
}

//-----------------------------------------------------------------------------

gpu_addr ClientGrid::GetInactiveGrid()
{
    return this->m_d_grid1 == this->m_d_activegrid ? 
        this->m_d_grid2 : 
        this->m_d_grid1;
}

//-----------------------------------------------------------------------------

void ClientGrid::indices_from_position(f3& position, unsigned* x,
    unsigned* y, unsigned* z)
{
    cuda_grid cpu_tmp;
    cuda::host::GetGrid(this->m_d_activegrid, (cpu_addr*)&cpu_tmp);

    float cellsize = cpu_tmp.m_cellsize;
    float half_dim = (this->m_dim_cells * cellsize) / 2;
    if (!(position.x >= -half_dim && position.x <= half_dim))
    {
        int i = 0;
        i++;
    }
    if (!(position.y >= -half_dim && position.y <= half_dim))
    {
        int i = 0;
        i++;
    }
    if (!(position.z >= -half_dim && position.z <= half_dim))
    {
        int i = 0;
        i++;
    }


    assert(position.x >= -half_dim && position.x <= half_dim);
    assert(position.y >= -half_dim && position.y <= half_dim);
    assert(position.z >= -half_dim && position.z <= half_dim);

    *x = static_cast<unsigned>((position.x + half_dim) / cellsize);
    *y = static_cast<unsigned>((position.y + half_dim) / cellsize);
    *z = static_cast<unsigned>((position.z + half_dim) / cellsize);
}