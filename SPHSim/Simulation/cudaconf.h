
#ifndef SIMULATION_CUDACONF
#define SIMULATION_CUDACONF

#define N_THREADS 512
#define THREAD_INDEX blockIdx.x * N_THREADS + threadIdx.x;

#endif