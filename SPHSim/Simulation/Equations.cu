
#include "Equations.cuh"

namespace cuda
{

//=============================================================================

    __device__ float device::gauss_W(float3 x_i, float3 x_j, float h)
    {
        float h2 = h*h;
        float3 x_ij = x_i - x_j;
        float norm2 = sqrt(x_ij.x*x_ij.x + x_ij.y*x_ij.y + x_ij.z*x_ij.z);

        // Leave if neighbor is not supported
        if (norm2 > RADIUS2(h))
        { return 0.0f; }

        float q2 = norm2 / h2;
        float f_q = exp(-q2);
        return (1 / (PI*h2) * f_q);
    }

    //-----------------------------------------------------------------------------

    __device__ float3 device::gauss_gradW(float3 x_i, float3 x_j, float h)
    {
        float3 grad;

        float delta = 1e-4f;
        float3 e1 = make_float3(1.0f*delta, 0.0*delta, 0.0*delta);
        float3 e2 = make_float3(0.0f*delta, 1.0*delta, 0.0*delta);
        float3 e3 = make_float3(0.0f*delta, 0.0*delta, 1.0*delta);
        float W = gauss_W(x_i, x_j, h);

        float dx1 = gauss_W(x_i + e1, x_j, h);
        float dy1 = gauss_W(x_i + e2, x_j, h);
        float dz1 = gauss_W(x_i + e3, x_j, h);
        float dx2 = gauss_W(x_i - e1, x_j, h);
        float dy2 = gauss_W(x_i - e2, x_j, h);
        float dz2 = gauss_W(x_i - e3, x_j, h);

        grad.x = (dx1 - dx2) / (2.0f * delta);
        grad.y = (dy1 - dy2) / (2.0f * delta);
        grad.z = (dz1 - dz2) / (2.0f * delta);
        return grad;
    }

//=============================================================================

    __device__ void particles_to_shmem(Particle* glob_particles, Particle* sh_particles, unsigned long n_particles)
    {
        unsigned index = threadIdx.x;

        // Copy over particles for all blocks
        for (unsigned i = 0; (i < gridDim.x) && ((index + (i*N_THREADS)) < n_particles); i++)
        { sh_particles[index + (i*N_THREADS)] = glob_particles[index + (i*N_THREADS)]; }
    }

    //-----------------------------------------------------------------------------

    __device__ void shmem_to_particles(Particle* glob_particles, Particle* sh_particles)
    {
        unsigned index = threadIdx.x;
        unsigned block = blockIdx.x;

        glob_particles[index + (block*N_THREADS)] = sh_particles[index + (block*N_THREADS)];
    }

//=============================================================================

    __global__ void kernel::calc_densities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;
        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            Particle* p_i = &particles[index];

            // Delete forces from previous iteration
            p_i->m_force.x = 0.0;
            p_i->m_force.y = 0.0;
            p_i->m_force.z = 0.0;

            unsigned long* indices;              // Takes indices of neighbors
            unsigned       n_indices = 0;        // Takes number of indices

            dim3 xyz;
            device::GetCellIndex(grid, p_i->m_position, &xyz);
            device::GetNeighbors(grid, xyz, &indices, &n_indices);

            // Iterate over all neighbors
            float rho_i = 0.0;
            for (unsigned i = 0; i < n_indices; i++)
            {
                Particle* p_j = &particles[indices[i]];

                f3& x_i = p_i->m_position;
                f3& x_j = p_j->m_position;

                float w = device::gauss_W(to_float3(x_i), to_float3(x_j), h);
                rho_i += w * mass;
            }

            p_i->m_density = rho_i;
            particles[index].m_density = rho_i;

            free(indices);
        }

    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_pressures(cuda_grid* grid, float mass, float h, float rho0, float k, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;
        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            Particle* p_i = &particles[index];

            float press_i = p_i->m_density / rho0;
            press_i = powf(press_i, 7.0f) - 1.0f;

            p_i->m_pressure = k * press_i;
        }
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_pressureforces(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;
        extern __shared__ Particle sh_particles[];

        if (index >= n_particles) { return; }

        particles_to_shmem(particles, sh_particles, n_particles);
        __syncthreads();

        Particle* particle_i = &sh_particles[index];
        if (particle_i->m_type == ParticleType::FLUID)
        {
            float m_i     = mass;
            float3 x_i    = to_float3(particle_i->m_position);
            float rho_i   = particle_i->m_density;
            float press_i = particle_i->m_pressure;



            unsigned long* indices;              // Takes indices of neighbors
            unsigned       n_indices = 0;        // Takes number of indices

            dim3 xyz;
            device::GetCellIndex(grid, particle_i->m_position, &xyz);
            device::GetNeighbors(grid, xyz, &indices, &n_indices);



            // Calculate pressure gradient at x_i
            float3 p_sum;
            for (unsigned i = 0; i < n_indices; i++)
            {
                unsigned long j = indices[i];

                Particle* particle_j = &sh_particles[j];
                float rho_j = particle_j->m_density;
                float p_j   = particle_j->m_pressure;
                float m_j   = mass;
                float3 x_j  = to_float3(particle_j->m_position);
                float3 x_ij = x_i - x_j;

                float3 gradW = device::gauss_gradW(x_i, x_j, h);

                // Gradient of p field
                float f = (press_i / (rho_i*rho_i)) + (p_j / (rho_j * rho_j));
                p_sum += factor(gradW, m_j * f);

            }

            float3 F_pressure = factor(p_sum, -m_i);
            particle_i->m_force.x += F_pressure.x;
            particle_i->m_force.y += F_pressure.y;
            particle_i->m_force.z += F_pressure.z;

            // Reset Z force to 0 for 2D simulation
            particle_i->m_force.z = 0.0;

            free(indices);
        }

        shmem_to_particles(particles, sh_particles);
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_viscosityforces(unsigned long n_particles, cuda_grid* grid, float mass, float h, float nu, Particle* particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;
        extern __shared__ Particle sh_particles[];

        if (index >= n_particles) { return; }

        particles_to_shmem(particles, sh_particles, n_particles);
        __syncthreads();

        Particle* particle_i = &sh_particles[index];
        if (particle_i->m_type == ParticleType::FLUID)
        {
            float3 x_i = to_float3(particle_i->m_position);
            float3 v_i = to_float3(particle_i->m_velocity);

            
            unsigned long* indices;              // Takes indices of neighbors
            unsigned       n_indices = 0;        // Takes number of indices

            dim3 xyz;
            device::GetCellIndex(grid, particle_i->m_position, &xyz);
            device::GetNeighbors(grid, xyz, &indices, &n_indices);


            float3 v_laplacian = make_float3(0, 0, 0);
            for (unsigned i = 0; i < n_indices; i++)
            {
                unsigned long j = indices[i];
                if (j == i) { continue; }

                Particle* particle_j = &sh_particles[j];
                float3 x_j  = to_float3(particle_j->m_position);
                float3 x_ij = x_i - x_j;
                float3 v_j  = to_float3(particle_j->m_velocity);
                float3 v_ij = v_i - v_j;
                float3 grad = device::gauss_gradW(x_i, x_j, h);
                float m_j = mass;
                float rho_j = particle_j->m_density;

                float h2 = h * h;
                float frac = dot(grad, x_ij) / (dot(x_ij, x_ij) + 0.01f * h2);

                // Laplacian of v field
                v_laplacian += factor(v_ij, m_j / rho_j * frac);
            }

            v_laplacian = factor(v_laplacian, 2);

            float3 F_visc = factor(v_laplacian, mass * nu);
            particle_i->m_force.x += F_visc.x;
            particle_i->m_force.y += F_visc.y;
            particle_i->m_force.z += F_visc.z;

            free(indices);
        }

        shmem_to_particles(particles, sh_particles);
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_gravityforces(float G, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;
        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            particles[index].m_force.y -= (G * mass);
        }
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_velocity_halfstep(float dt, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;
        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            float factor = dt / mass;

            particles[index].m_velocity.x += particles[index].m_force.x * factor;
            particles[index].m_velocity.y += particles[index].m_force.y * factor;
            particles[index].m_velocity.z += particles[index].m_force.z * factor;
        }
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::calc_velocities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;

        extern __shared__ Particle sh_particles[];

        if (index >= n_particles) { return; }

        particles_to_shmem(particles, sh_particles, n_particles);
        __syncthreads();

        if (particles[index].m_type == ParticleType::FLUID)
        {
            float epsilon = 0.05f;
            Particle* p_i = &particles[index];

            float3 v_i = to_float3(p_i->m_velocity);
            float3 x_i = to_float3(p_i->m_position);

            unsigned long* indices;              // Takes indices of neighbors
            unsigned       n_indices = 0;        // Takes number of indices

            dim3 xyz;
            device::GetCellIndex(grid, p_i->m_position, &xyz);
            device::GetNeighbors(grid, xyz, &indices, &n_indices);



            float3 sum;
            for (unsigned i = 0; i < n_indices; i++)
            {
                unsigned long j = indices[i];
                if (j == index) { continue; }

                Particle* p_j = &particles[j];
                float     rho_j = p_j->m_density;
                float3    v_j = to_float3(p_j->m_velocity);
                float3    x_j = to_float3(p_j->m_position);
                float3    v_ij = v_j - v_i;
                float     m_j = mass;

                float W = device::gauss_W(x_i, x_j, h);
                sum += factor(v_ij, (m_j*W) / rho_j);
            }

            float3 new_velocity = to_float3(p_i->m_velocity) + factor(sum, epsilon);
            p_i->m_velocity.x = new_velocity.x;
            p_i->m_velocity.y = new_velocity.y;
            p_i->m_velocity.z = new_velocity.z;

            free(indices);
        }

        shmem_to_particles(particles, sh_particles);
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::apply_particleupdates(float dt, Particle* particles, float3* positions, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;

        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            float3 new_position;
            new_position.x = particles[index].m_position.x + particles[index].m_velocity.x * dt;
            new_position.y = particles[index].m_position.y + particles[index].m_velocity.y * dt;
            new_position.z = particles[index].m_position.z + particles[index].m_velocity.z * dt;

            particles[index].m_position.x = new_position.x;
            particles[index].m_position.y = new_position.y;
            particles[index].m_position.z = new_position.z;
            positions[index] = make_float3(new_position.x, new_position.y, new_position.z);
        }
    }

    //-------------------------------------------------------------------------

    __global__ void kernel::grid_update(cuda_grid* inactive_grid, Particle* particles, unsigned long n_particles)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;;

        if (index < n_particles && particles[index].m_type == ParticleType::FLUID)
        {
            device::InsertGrid(inactive_grid, index, particles[index].m_position);
        }
    }

//=============================================================================

    void run::calc_densities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_densities <<<blocks, N_THREADS >>>(grid, mass, h, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_pressures(cuda_grid* grid, float mass, float h, float rho0, float k, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_pressures <<<blocks, N_THREADS >>>(grid, mass, h, rho0, k, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_pressureforces(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_pressureforces <<<blocks, N_THREADS, n_particles*sizeof(Particle) >>>(grid, mass, h, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_viscosityforces(cuda_grid* grid, float mass, float h, float nu, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_viscosityforces <<<blocks, N_THREADS, n_particles*sizeof(Particle) >>>(n_particles, grid, mass, h, nu, particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_gravityforces(float G, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles)
    { 
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_gravityforces <<<blocks, N_THREADS >>>(G, mass, grid, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_velocity_halfstep(float dt, float mass, cuda_grid* grid, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_velocity_halfstep <<<blocks, N_THREADS >>>(dt, mass, grid, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::calc_velocities(cuda_grid* grid, float mass, float h, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::calc_velocities << <blocks, N_THREADS, n_particles*sizeof(Particle) >> >(grid, mass, h, particles, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::apply_particleupdates(float dt, Particle* particles, float3* positions, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::apply_particleupdates <<< blocks, N_THREADS >>>(dt, particles, positions, n_particles);
        check(cudaGetLastError());
    }

    //-------------------------------------------------------------------------

    void run::grid_update(cuda_grid* inactive_grid, Particle* particles, unsigned long n_particles)
    {
        size_t blocks = static_cast<size_t>(n_particles / N_THREADS) + 1;
        kernel::grid_update <<< blocks, N_THREADS >>>(inactive_grid, particles, n_particles);
        check(cudaGetLastError());
    }

}