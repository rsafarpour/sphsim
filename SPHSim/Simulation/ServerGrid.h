
#ifndef SIMULATION_GRID_SERVERGRID
#define SIMULATION_GRID_SERVERGRID

#include <algorithm>
#include <assert.h>
#include <f3.h>
#include <vector>

#include "Grid.h"
#include "Particle.h"
#include "ParticleGenerator.h"
#include "ServerCell.h"

class ServerGrid : public Grid
{
public:
    ServerGrid();
    ~ServerGrid();

    virtual void     Clear()                                          override;
    virtual void     Init(float dim, float min_cellsize)              override;
    virtual void     Insert(unsigned long i, f3& position)            override;
    virtual void     Update(unsigned long i, f3 from, f3 to)          override;
    virtual Cell*    GetCell(unsigned x, unsigned y, unsigned z)      override;
    virtual Cell*    GetCell(f3& position)                            override;
    virtual f3       GetMax()                                         override;
    virtual f3       GetMin()                                         override;
    virtual CellList GetNeighbors(unsigned x, unsigned y, unsigned z) override;
    virtual CellList GetNeighbors(f3& position)                       override;

private:
    void indices_from_position(
        f3& position, 
        unsigned* x, 
        unsigned* y, 
        unsigned* z);

    unsigned    m_n_cells;
    ServerCell* m_cells;
    float       m_cellsize;
    unsigned    m_dim_cells;
};

#endif