
#ifndef SIMULATION_GRID_CELL
#define SIMULATION_GRID_CELL

#include <memory>
#include <vector>

class Cell;
typedef std::vector<unsigned long> IndexList;
typedef std::vector<Cell*> CellList;

class Cell
{
public:
    virtual void      Add(unsigned long index)      = 0;
    virtual void      Clear()                       = 0;
    virtual bool      Contains(unsigned long index) = 0;
    virtual void      Remove(unsigned long index)   = 0;
    virtual IndexList GetIndices()                  = 0;
};

#endif