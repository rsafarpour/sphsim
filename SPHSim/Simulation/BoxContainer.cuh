
#ifndef SIMULATION_CUDA_COXCONTAINER
#define SIMULATION_CUDA_BOXCONTAINER

#include <cuda.h>
#include <cuda_runtime.h>

#include <Plane.h>

#include "cudaconf.h"
#include "float3.cuh"
#include "Particle.h"

namespace cuda
{
    namespace kernel
    {
        __global__  void clip(Plane* planes, Particle* particles, unsigned long n_particles);

    }

    namespace run
    {
        void clip(Plane* planes, Particle* particles, unsigned long n_particles);
    }


    __host__ __device__  void projection(Plane* plane, f3* position);
}

#endif
