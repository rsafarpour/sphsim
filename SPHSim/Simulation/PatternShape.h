
#ifndef SIMULATION_PATTERNSHAPE
#define SIMULATION_PATTERNSHAPE

#include <f3.h>

class PatternShape
{
public:
    virtual f3 GetMin()              const = 0;
    virtual f3 GetMax()              const = 0;
    virtual bool IsInside(f3& point) const = 0;
};

#endif