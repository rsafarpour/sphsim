
#ifndef SIMULATION_BOXCONTAINERGENERATOR
#define SIMULATION_BOXCONTAINERGENERATOR

#include <assert.h>
#include <vector>

#include "BoxGenerator2D.h"
#include "PatternShape.h"
#include "PoissonDiskSampler2D.h"
#include "Properties.h"

class BoxContainerGenerator2D
  : public ParticleGenerator,
    public PatternShape
{
public:
    BoxContainerGenerator2D(
        f3 min_inner, 
        f3 max_inner,
        f3 min_outer,
        f3 max_outer);

    virtual f3   GetMin()                                       const override;
    virtual f3   GetMax()                                       const override;
    virtual void GetParticles(float h, ParticleList* particles) const override;
    virtual bool IsInside(f3& point)                            const override;
    virtual void SetDomain(f3 min, f3 max)                            override;

private:

    float          m_particle_mass;
    float          m_kernelsupport;
    BoxGenerator2D m_box_inner;
    BoxGenerator2D m_box_outer;

};

#endif