
#ifndef SIMULATION_PARTICLEGENERATOR
#define SIMULATION_PARTICLEGENERATOR

#include <f3.h>

#include "Particle.h"

#define SAMPLE_RADIUS(h) 0.92f*h

class ParticleGenerator
{
public:
    virtual void GetParticles(float h, ParticleList* particles) const = 0;
    virtual void SetDomain(f3 min, f3 max)                            = 0;
};

#endif