#include "BoxContainerGenerator2D.h"

BoxContainerGenerator2D::BoxContainerGenerator2D(
    f3 min_inner,
    f3 max_inner,
    f3 min_outer,
    f3 max_outer) 
  : m_box_inner(min_inner, max_inner),
    m_box_outer(min_outer, max_outer)

{

}

//-----------------------------------------------------------------------------

f3 BoxContainerGenerator2D::GetMin() const
{
    return this->m_box_outer.GetMin();
}

//-----------------------------------------------------------------------------

f3 BoxContainerGenerator2D::GetMax() const
{
    return this->m_box_outer.GetMax();
}

//-----------------------------------------------------------------------------

void BoxContainerGenerator2D::GetParticles(float h, ParticleList* particles) const
{
    
    this->m_box_inner.GetSurfaceParticles(particles, h);
    this->m_box_outer.GetSurfaceParticles(particles, h);

    PoissonDiskSampler2D sampler(*this, SAMPLE_RADIUS(h));
    sampler.Sample(particles);

    // Tag all particles as solid particles
    for (Particle& p : *particles)
    { p.m_type = ParticleType::SOLID; }
}

//-----------------------------------------------------------------------------

bool BoxContainerGenerator2D::IsInside(f3& point) const
{
    if      (this->m_box_inner.IsInside(point)) { return false; }
    else if (this->m_box_outer.IsInside(point)) { return true; }

    return false;
}

//-----------------------------------------------------------------------------

void BoxContainerGenerator2D::SetDomain(f3 min, f3 max)
{
    this->m_box_inner.SetDomain(min, max);
    this->m_box_outer.SetDomain(min, max);
}
