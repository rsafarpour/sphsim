
#ifndef SIMULATION_CUDA_GRID
#define SIMULATION_CUDA_GRID

#include <cuda.h>
#include <cuda_runtime.h>
#include <CudaDbg.h>
#include <device_functions.h>
#include <f3.h>

#include "cudaconf.h"
#include "vmemory.cuh"

#define CELL_SIZE 150
#define NEIGHBORHOOD_SIZE 27

typedef unsigned long cuda_ul;

struct cuda_cell
{ 
    int     n_indices;
    cuda_ul indices[CELL_SIZE]; 

    cuda_cell() : n_indices(0) {};
};

struct cuda_grid
{
    f3       m_min;
    f3       m_max;
    long     m_dim_cells;
    gpu_addr m_cells;
    float    m_cellsize;
};

namespace cuda
{

    namespace kernel
    {
        __global__ void ClearGrid(gpu_addr grid, long n_cells);
    }

    namespace host
    {
        __host__ void GetGrid(gpu_addr grid, cpu_addr out);
        __host__ void CreateGrid(float dim, float min_cellsize, gpu_addr* grid);
        __host__ void DestroyGrid(gpu_addr grid);
        __host__ void ClearGrid(gpu_addr grid, long dim_cells);
        __host__ void InsertGrid(gpu_addr grid, unsigned long index, f3 position);
        __host__ void InsertGrid(gpu_addr grid, unsigned long index, gpu_addr cell);
        __host__ void GetCell(gpu_addr grid, f3 position, cpu_addr cell);
        __host__ void GetCell(gpu_addr grid, uint3 position, cpu_addr cell);
        __host__ void GetCell(gpu_addr grid, unsigned long i, cpu_addr cell);
        __host__ void SetCell(gpu_addr grid, f3 position, cpu_addr cell);
        __host__ void SetCell(gpu_addr grid, uint3 position, cpu_addr cell);
        __host__ void SetCell(gpu_addr grid, unsigned long position, cpu_addr cell);
        __host__ void Contains(unsigned long index, gpu_addr cell, bool* contains);
        __host__ void GetIndices(gpu_addr cell, unsigned long** indices, unsigned* n_indices);
    }

    namespace device
    {
        __device__ void InsertGrid(gpu_addr grid, unsigned long index, f3 position);
        __device__ void InsertGrid(gpu_addr grid, unsigned long index, gpu_addr cell);
        __device__ void GetNeighbors(gpu_addr grid, dim3 cell, unsigned long** indices, unsigned* n);
        __device__ void GetCellIndex(gpu_addr grid, f3& position, dim3* index);
        __device__ void ClearGrid(gpu_addr grid);
    }
}

#endif