#include "vmemory.cuh"

namespace cuda
{
    gpu_addr vram::allocate(size_t bytes)
    {
        void* rval;
        check(cudaMalloc(&rval, bytes));
        return rval;
    }

    //-----------------------------------------------------------------------------

    void  vram::free(gpu_addr addr)
    { check(cudaFree((void*)addr)); }

    //-----------------------------------------------------------------------------

    void vram::transfer_in(cpu_addr from, gpu_addr to, size_t bytes)
    { check(cudaMemcpy(to, from, bytes, cudaMemcpyHostToDevice)); }

    //-----------------------------------------------------------------------------

    void vram::transfer_out(gpu_addr from, cpu_addr to, size_t bytes)
    { check(cudaMemcpy(to, from, bytes, cudaMemcpyDeviceToHost)); }
}