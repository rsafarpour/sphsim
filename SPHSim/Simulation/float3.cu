#include "float3.cuh"

void test()
{}

__host__ __device__ void operator+=(float3& a, const float3& b)
{ a = a + b; }

//-----------------------------------------------------------------------------

__host__ __device__ float3 operator+(const float3& a, const float3& b)
{
    float3 retval;
    retval.x = a.x + b.x;
    retval.y = a.y + b.y;
    retval.z = a.z + b.z;
    return retval;
}

__host__ __device__ float3 operator-(const float3& a, const float3& b)
{
    float3 retval;
    retval.x = a.x - b.x;
    retval.y = a.y - b.y;
    retval.z = a.z - b.z;
    return retval;
}

//-----------------------------------------------------------------------------

__host__ __device__ void operator-=(float3& a, const float3& b)
{ a = a - b; }

//-----------------------------------------------------------------------------

__host__ __device__ float3 factor(float3& a, const float& b)
{
    float3 retval;
    retval.x = a.x * b;
    retval.y = a.y * b;
    retval.z = a.z * b;
    return retval;
}

//-----------------------------------------------------------------------------

__host__ __device__ float dot(const float3& a, const float3& b)
{
    float retval;
    retval = a.x * b.x;
    retval += a.y * b.y;
    retval += a.z * b.z;
    return retval;
}

//-----------------------------------------------------------------------------

__host__ __device__ float3 to_float3(const f3& a)
{ return make_float3(a.x, a.y, a.z); }