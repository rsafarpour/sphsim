
#include "ClientCell.h"

ClientCell::ClientCell(gpu_addr grid, gpu_addr c) 
  : m_cell(c),
    m_grid(grid)
{
    assert(c != nullptr);
}

//-----------------------------------------------------------------------------

void ClientCell::Add(unsigned long index)
{
    cuda::host::InsertGrid(this->m_grid, index, this->m_cell);
}

//-----------------------------------------------------------------------------

void ClientCell::Clear()
{
    return;
}

//-----------------------------------------------------------------------------

bool ClientCell::Contains(unsigned long index)
{
    bool contains = false;
    cuda::host::Contains(index, this->m_cell, &contains);

    return contains;
}

//-----------------------------------------------------------------------------

void ClientCell::Remove(unsigned long index)
{
    return;
}

//-----------------------------------------------------------------------------

IndexList ClientCell::GetIndices()
{
    IndexList ilist;

    unsigned long indices[CELL_SIZE];
    unsigned n_indices;
    cuda::host::GetIndices(this->m_cell, (unsigned long**)&indices, &n_indices);

    for (unsigned i = 0; i < n_indices; i++)
    { ilist.push_back(indices[i]); }

    return ilist;
}