
#ifndef SIMULATION_CONTAINER
#define SIMULATION_CONTAINER

#include "Particle.h"

class Container
{
public:
    virtual void clip(Particle* particles, unsigned long n_particles) = 0;
};

#endif