
#include "Grid.cuh"

// HELPER FUNCTIONS
__host__ __device__ unsigned xyz_to_i(gpu_addr grid_addr, unsigned x, unsigned y, unsigned z)
{
    cuda_grid* grid;

#ifdef __CUDA_ARCH__
    grid = static_cast<cuda_grid*>(grid_addr);
#else
    cuda_grid cpu_tmp;
    cuda::host::GetGrid(grid_addr, (cpu_addr)&cpu_tmp);
    grid = &cpu_tmp;
#endif

    unsigned long i = x;
    i += grid->m_dim_cells * y;
    i += grid->m_dim_cells * grid->m_dim_cells * z;
    return i;
}

//-----------------------------------------------------------------------------

__host__ __device__ dim3 position_to_xyz(gpu_addr grid_addr, f3& position)
{
    cuda_grid* grid;

#ifdef __CUDA_ARCH__
    grid = static_cast<cuda_grid*>(grid_addr);
#else
    cuda_grid cpu_tmp;
    cuda::host::GetGrid(grid_addr, (cpu_addr)&cpu_tmp);
    grid = &cpu_tmp;
#endif

    float cellsize = grid->m_cellsize;
    float half_dim = (grid->m_dim_cells * cellsize) / 2;

    assert(position.x >= -half_dim && position.x <= half_dim);
    assert(position.y >= -half_dim && position.y <= half_dim);
    assert(position.z >= -half_dim && position.z <= half_dim);

    dim3 xyz;
    xyz.x = static_cast<unsigned>((position.x + half_dim) / cellsize);
    xyz.y = static_cast<unsigned>((position.y + half_dim) / cellsize);
    xyz.z = static_cast<unsigned>((position.z + half_dim) / cellsize);
    return xyz;
}

//-----------------------------------------------------------------------------

__host__ __device__ unsigned long position_to_index(gpu_addr grid_addr, f3& position)
{
    dim3 xyz = position_to_xyz(grid_addr, position);
    return xyz_to_i(grid_addr, xyz.x, xyz.y, xyz.z);
}

//=============================================================================

namespace cuda
{

    __global__ void kernel::ClearGrid(gpu_addr grid, long n_cells)
    {
        unsigned index = blockIdx.x * N_THREADS + threadIdx.x;
        cuda_grid* d_grid = static_cast<cuda_grid*>(grid);

        if (index < n_cells)
        {
            cuda_cell* cell = &static_cast<cuda_cell*>(d_grid->m_cells)[index];
            cell->n_indices = 0;
        }
    }

//-----------------------------------------------------------------------------

    __host__ void host::GetGrid(gpu_addr grid, cpu_addr out)
    {
        vram::transfer_out(grid, out, sizeof(cuda_grid));
    }

//-----------------------------------------------------------------------------

    __host__ void host::CreateGrid(float dim, float min_cellsize, gpu_addr* grid)
    {
        // Calculate number of cells per dimension
        cuda_grid cpu_tmp;
        cpu_tmp.m_dim_cells = static_cast<unsigned>(ceil(dim / min_cellsize));
        cpu_tmp.m_dim_cells -= cpu_tmp.m_dim_cells % 2;

        cpu_tmp.m_cellsize = dim / (cpu_tmp.m_dim_cells - 1);
        cpu_tmp.m_max = cpu_tmp.m_dim_cells / 2 * cpu_tmp.m_cellsize;
        cpu_tmp.m_min = -cpu_tmp.m_max;

        // Total number of cells
        unsigned long n_cells = cpu_tmp.m_dim_cells * cpu_tmp.m_dim_cells * cpu_tmp.m_dim_cells;

        // allocate grid space on client
        gpu_addr dst = vram::allocate(sizeof(cuda_grid));
        cpu_tmp.m_cells = vram::allocate(n_cells * sizeof(cuda_cell));

        // Transfer construction object to device
        cpu_addr src = &cpu_tmp;
        vram::transfer_in(src, dst, sizeof(cuda_grid));

        *grid = dst;
    }

//-----------------------------------------------------------------------------

    __host__ void host::DestroyGrid(gpu_addr grid)
    {
        cuda_grid cpu_tmp;
        vram::transfer_out(grid, &cpu_tmp, sizeof(cuda_grid));
        
        vram::free(cpu_tmp.m_cells);
        vram::free(grid);
    }

//-----------------------------------------------------------------------------

    __host__ void host::ClearGrid(gpu_addr grid, long dim_cells)
    {
        long n_cells = dim_cells*dim_cells*dim_cells;

        size_t blocks = n_cells % N_THREADS + 1;
        kernel::ClearGrid <<<blocks, N_THREADS>>>(grid, n_cells);
    }

//-----------------------------------------------------------------------------

    __host__ void host::InsertGrid(gpu_addr grid, unsigned long index, f3 position)
    {
        cuda_cell cpu_tmp_cell;
        host::GetCell(grid, position, &cpu_tmp_cell);

        assert(cpu_tmp_cell.n_indices < CELL_SIZE);
        cpu_tmp_cell.indices[cpu_tmp_cell.n_indices] = index;
        cpu_tmp_cell.n_indices++;

        host::SetCell(grid, position, &cpu_tmp_cell);
    }

//-----------------------------------------------------------------------------

    __host__ void host::InsertGrid(gpu_addr grid, unsigned long index, gpu_addr cell)
    {
        cuda_cell cpu_tmp_cell;
        vram::transfer_out(cell, &cpu_tmp_cell, sizeof(cuda_cell));

        assert(cpu_tmp_cell.n_indices < CELL_SIZE);
        cpu_tmp_cell.indices[cpu_tmp_cell.n_indices] = index;
        cpu_tmp_cell.n_indices++;

        vram::transfer_in(&cpu_tmp_cell, cell, sizeof(cuda_cell));
    }

//-----------------------------------------------------------------------------

    __host__ void host::GetCell(gpu_addr grid, f3 position, cpu_addr cell)
    {
        cuda_grid cpu_tmp;
        vram::transfer_out(grid, &cpu_tmp, sizeof(cuda_grid));

        unsigned long i = position_to_index(grid, position);

        host::GetCell(grid, i, cell);
    }

//-----------------------------------------------------------------------------

    __host__ void host::GetCell(gpu_addr grid, uint3 position, cpu_addr cell)
    {
        unsigned long i = xyz_to_i(grid, position.x, position.y, position.z);
        host::GetCell(grid, i, cell);
    }

//-----------------------------------------------------------------------------

    __host__ void host::GetCell(gpu_addr grid, unsigned long i, cpu_addr cell)
    {
        cuda_grid cpu_tmp_grid;
        vram::transfer_out(grid, &cpu_tmp_grid, sizeof(cuda_grid));

        cuda_cell* cells = static_cast<cuda_cell*>(cpu_tmp_grid.m_cells);
        gpu_addr src = static_cast<gpu_addr>(&cells[i]);
        cpu_addr dst = cell;
        vram::transfer_out(src, dst, sizeof(cuda_cell));
    }

//-----------------------------------------------------------------------------

    __host__ void host::SetCell(gpu_addr grid, f3 position, cpu_addr cell)
    {
        cuda_grid cpu_tmp;
        vram::transfer_out(grid, &cpu_tmp, sizeof(cuda_grid));

        cuda_grid* dev_grid = static_cast<cuda_grid*>(grid);
        unsigned long i = position_to_index(dev_grid, position);

        host::SetCell(grid, i, cell);
    }

//-----------------------------------------------------------------------------

    __host__ void host::SetCell(gpu_addr grid, uint3 position, cpu_addr cell)
    {
        unsigned long i = xyz_to_i(grid, position.x, position.y, position.z);
        SetCell(grid, i, cell);
    }

//-----------------------------------------------------------------------------

    __host__ void host::SetCell(gpu_addr grid, unsigned long i, cpu_addr cell)
    {
        cuda_grid cpu_tmp_grid;
        vram::transfer_out(grid, &cpu_tmp_grid, sizeof(cuda_grid));

        cuda_cell* cells = static_cast<cuda_cell*>(cpu_tmp_grid.m_cells);
        gpu_addr dst = static_cast<gpu_addr>(&cells[i]);
        cpu_addr src = cell;
        vram::transfer_in(src, dst, sizeof(cuda_cell));
    }

    //-----------------------------------------------------------------------------

    __host__ void host::Contains(unsigned long index, gpu_addr cell, bool* contains)
    {
        unsigned long indices[CELL_SIZE];
        unsigned n_indices;
        GetIndices(cell, (unsigned long**)&indices, &n_indices);

        for (unsigned i = 0; i < n_indices; i++)
        {
            if (indices[i] == index)
            {
                *contains = true;
                return;
            }
        }

        *contains = false;
    }

    //-----------------------------------------------------------------------------

    __host__ void host::GetIndices(gpu_addr cell, unsigned long** indices, unsigned* n_indices)
    {
        cuda_cell cpu_tmp;
        vram::transfer_out(cell, &cpu_tmp, sizeof(cuda_cell));

        *indices = cpu_tmp.indices;
        *n_indices = cpu_tmp.n_indices;
    }

    //-----------------------------------------------------------------------------

    __device__ void device::InsertGrid(gpu_addr grid, unsigned long index, f3 position)
    {
        cuda_grid* gpu_grid = static_cast<cuda_grid*>(grid);
        unsigned long i_cell = position_to_index(gpu_grid, position);

        cuda_cell* cells = static_cast<cuda_cell*>(gpu_grid->m_cells);

        int i_index = atomicAdd(&cells[i_cell].n_indices, 1);
        cells[i_cell].indices[i_index] = index;
    }

    //-----------------------------------------------------------------------------

    __device__ void device::InsertGrid(gpu_addr grid, unsigned long index, gpu_addr cell)
    {
        cuda_cell* gpu_cell = static_cast<cuda_cell*>(cell);

        int i_index = atomicAdd(&gpu_cell->n_indices, 1);
        gpu_cell->indices[i_index] = index;
    }

    //-----------------------------------------------------------------------------

    __device__ void device::GetNeighbors(gpu_addr grid, dim3 cell, unsigned long** indices, unsigned* n)
    {
        cuda_grid* gpu_grid = static_cast<cuda_grid*>(grid);
        cuda_cell* gpu_cells = static_cast<cuda_cell*>(gpu_grid->m_cells);

        cuda_cell* neighborhood[NEIGHBORHOOD_SIZE];
        unsigned x_min = max((int)cell.x - 1, 0);
        unsigned x_max = min((int)cell.x + 1, (int)gpu_grid->m_dim_cells - 1);
        unsigned y_min = max((int)cell.y - 1, 0);
        unsigned y_max = min((int)cell.y + 1, (int)gpu_grid->m_dim_cells - 1);
        unsigned z_min = max((int)cell.z - 1, 0);
        unsigned z_max = min((int)cell.z + 1, (int)gpu_grid->m_dim_cells - 1);
        
        // Find neighboring cells
        unsigned index = 0;
        for (unsigned i = x_min; i <= x_max; i++)
        {
            for (unsigned j = y_min; j <= y_max; j++)
            {
                for (unsigned k = z_min; k <= z_max; k++)
                {
                    unsigned neighbor_index = xyz_to_i(grid, i, j, k);
                    neighborhood[index] = &gpu_cells[neighbor_index];
                    index++;
                }
            }
        }

        // Create index array
        unsigned l_n = 0;
        for (unsigned i = 0; i < NEIGHBORHOOD_SIZE; i++)
        { l_n += neighborhood[i]->n_indices; }

        unsigned long* l_indices = static_cast<unsigned long*>(malloc(sizeof(unsigned long) *l_n));

        size_t offset = 0;
        for (unsigned i = 0; i < NEIGHBORHOOD_SIZE; i++)
            for (unsigned j = 0; j < neighborhood[i]->n_indices; j++, offset++)
            { l_indices[offset] = neighborhood[i]->indices[j]; }

        *n = l_n;
        *indices = l_indices;
    }

    //-----------------------------------------------------------------------------

    __device__ void device::GetCellIndex(gpu_addr grid, f3& position, dim3* index)
    {
        *index = position_to_xyz(grid,  position);
    }
}
