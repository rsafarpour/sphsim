
#ifndef SIMULATION_GAUSSKERNEL
#define SIMULATION_GAUSSKERNEL

#include <math.h>
#include <constants.h>

#include "Kernel.h"

#define RADIUS(h)  3.0f*h
#define RADIUS2(h) 3.0f*h*h

class GaussKernel2D : public Kernel
{
public:
    virtual f3    gradW(f3 x_i, f3 x_j, float h) override;
    virtual float W(f3 x_i, f3 x_j, float h)     override;
};

#endif