
#ifndef SIMULATION_SIMULATIONWIDGET
#define SIMULATION_SIMULATIONWIDGET

#include <Widget.h>
#include <imgui.h>

#include "Properties.h"

class Simulation;
class SimulationWidget : public Widget
{
    friend class Simulation;
public:
    SimulationWidget(Simulation& sim);

    virtual void Definition() override;

private:
    Properties  m_props;
    Simulation& m_sim;
};

#endif