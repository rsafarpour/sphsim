#include "VisualizationWidget.h"
#include "ParticleVisualizer.h"

VisualizationWidget::VisualizationWidget(ParticleVisualizer& vis)
  : m_particle_size(1.0f),
    m_vis(vis)
{}

//-----------------------------------------------------------------------------

void VisualizationWidget::Definition()
{
    ImGui::SetNextWindowPos(ImVec2(5.0, 210.0), ImGuiSetCond_Once);
    ImGui::SetNextWindowSize(ImVec2(155, 75), ImGuiSetCond_Always);
    ImGui::Begin("Visualization", nullptr,
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_NoMove);


    ImGui::InputFloat("Size", &this->m_particle_size, 0.1f, 0.5f, 2);
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::Text("Radius of particles");
        ImGui::EndTooltip();
    }

    if (ImGui::Button("Apply"))
        this->m_vis.render_impostor();


    ImGui::End();
}