#version 450
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
//layout(points, max_vertices = 4) out;

// ==== Inputs ====
in gl_PerVertex
{
	vec4  gl_Position;
} gl_in[];

in mat4 gView[];
in mat4 gProjection[];


// ==== Outputs ====
out vec2 ps_uv;


// ==== Uniforms ====
uniform float size = 0.5;
uniform vec3 cam_pos;

void main()
{
	// Calculate view projection matrix
	mat4 vp = gProjection[0] * gView[0];

	// Calculating camera frame
	mat4 view = gView[0];
	vec3 up = vec3(view[0][1], 
                   view[1][1], 
                   view[2][1]);

	//vec3 right = vec3(view[0][0], 
    //                  view[1][0], 
    //                  view[2][0]);

	vec3 to_cam = cam_pos - gl_Position.xyz;
	vec3 right = normalize(cross(to_cam, up));
	up         = normalize(up);




	vec3 lr_offset = right * size;
	vec3 tb_offset = up * size;

	ps_uv = vec2(1.0, 0.0);
	gl_Position = vp * vec4(gl_in[0].gl_Position.xyz - lr_offset + tb_offset, 1.0);
	EmitVertex();
	
	ps_uv = vec2(0.0, 0.0);
	gl_Position = vp * vec4(gl_in[0].gl_Position.xyz + lr_offset + tb_offset, 1.0);
	EmitVertex();

	ps_uv = vec2(1.0, 1.0);
	gl_Position = vp * vec4(gl_in[0].gl_Position.xyz - lr_offset - tb_offset, 1.0);
	EmitVertex();

	ps_uv = vec2(0.0, 1.0);
	gl_Position = vp * vec4(gl_in[0].gl_Position.xyz + lr_offset - tb_offset, 1.0);
	EmitVertex();
	
	EndPrimitive();
}