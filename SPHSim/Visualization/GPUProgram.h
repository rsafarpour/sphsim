
#ifndef VIZUALIZATION_GPUPROGRAM
#define VIZUALIZATION_GPUPROGRAM

#include <Dbg.h>
#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <string>
#include <map>

#include "Shader.h"
#include "Texture2D.h"

enum class uniform_t
{
    UNIFORM_FLOAT,
    UNIFORM_UINT,
    UNIFORM_INT,
    UNIFORM_2x3,
    UNIFORM_3x4,
    UNIFORM_4x4,
    UNIFORM_VEC2,
    UNIFORM_VEC3,
    UNIFORM_VEC4
};

class GPUProgram
{
public:
    GPUProgram();

    void        DisableAttribute(const char* name);
    void        EnableAttribute(const char* name);
    void        GeometryShaderFile(const char* path);
    std::string GetError();
    void        Link();
    void        MakeCurrent();
    void        PixelShaderFile(const char* path);
    void        SetAttribute(const char* name, unsigned components, unsigned bytes, void* data);
    void        SetTextureSampler(const char* name, Texture2D& tex, unsigned unit);
    void        SetUniform(const char* name, uniform_t type, void* data);
    void        VertexShaderFile(const char* path);

private:
    void   addingshader();
    void   attach(Shader* s);
    GLuint attrib_location(const char* name);
    void   clean_errors();
    void   detach(Shader* s);

    std::map<std::string, GLint> m_attributes;
    GeometryShader               m_gshader;
    bool                         m_hasvs;
    bool                         m_hasgs;
    bool                         m_hasps;
    bool                         m_islinked;
    GLuint                       m_program;
    PixelShader                  m_pshader;
    VertexShader                 m_vshader;
};

#endif