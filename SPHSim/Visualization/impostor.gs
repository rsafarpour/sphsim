#version 450
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

// ==== Input ====
in gl_PerVertex
{
	vec4  gl_Position;
} gl_in[];

// ==== Outputs ====
out vec2 ps_uv;

// ==== Uniforms ====
uniform uint dim;
uniform float max_size = 1.0;
uniform float desired_size;

void main()
{
	float size = desired_size/dim;

    if(size > max_size)
	{ size = max_size; }

	vec3 pos = gl_in[0].gl_Position.xyz;
    size = size/2;

	ps_uv = vec2(0.0, 1.0);
	gl_Position = vec4(pos.x - size, pos.y + size, 0.0, 1.0);
	EmitVertex();
	
	ps_uv = vec2(1.0, 1.0);
	gl_Position = vec4(pos.x + size, pos.y + size, 0.0, 1.0);
	EmitVertex();
	
	ps_uv = vec2(0.0, 0.0);
	gl_Position = vec4(pos.x - size, pos.y - size, 0.0, 1.0);
	EmitVertex();
	
	ps_uv = vec2(1.0, 0.0);
	gl_Position = vec4(pos.x + size, pos.y - size, 0.0, 1.0);
	EmitVertex();
	
	EndPrimitive();
}