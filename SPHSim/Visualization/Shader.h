
#ifndef VISUALIZATION_SHADER
#define VISUALIZATION_SHADER

#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

#include <fstream>
#include <string>


//=============================================================================

class Shader
{
public:
    Shader();

    void   Clear();
    GLuint GetID();
    void   SetSource(std::string src);
    void   SourceFromFile(std::string file);
    void   Compile();

protected:
    virtual void compile_impl() = 0;

    bool        m_iscompiled;
    GLuint      m_compiled;
    std::string m_code;
};

//=============================================================================

class VertexShader : public Shader
{
private:
    virtual void compile_impl() override;
};

//=============================================================================

class GeometryShader : public Shader
{
private:
    virtual void compile_impl() override;
};

//=============================================================================

class PixelShader : public Shader
{
private:
    virtual void compile_impl() override;
};

#endif