
#ifndef VISUALIZATION_FRAMEBUFFER
#define VISUALIZATION_FRAMEBUFFER

class Framebuffer
{
public:
    virtual void MakeCurrent() = 0;
};

#endif