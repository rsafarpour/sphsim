
#include "RenderObject.h"

RenderObject::RenderObject(bool instanced)
  : m_instanced(instanced),
    m_mesh(&this->m_program)
{}

//-----------------------------------------------------------------------------

void RenderObject::Draw()
{
    this->MakeCurrent();
    this->m_mesh.Draw();
}

//-----------------------------------------------------------------------------

GPUProgram&  RenderObject::GPUData()
{
    return this->m_program;
}

//-----------------------------------------------------------------------------

void RenderObject::MakeCurrent()
{
    this->m_program.MakeCurrent();
    this->m_mesh.MakeCurrent();
}

//-----------------------------------------------------------------------------

Mesh& RenderObject::MeshData()
{
    return this->m_mesh;
}