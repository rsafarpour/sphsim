
#ifndef VISUALIZATION_SCREENBUFFER
#define VISUALIZATION_SCREENBUFFER

#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

#include "Framebuffer.h"

class Screenbuffer : public Framebuffer
{
public:
    Screenbuffer(unsigned height, unsigned width);

    void Resize(unsigned height, unsigned width);
    void MakeCurrent() override;

private:
    unsigned m_screenx;
    unsigned m_screeny;
};

#endif