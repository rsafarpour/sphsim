
#include "Texture2D.h"

Texture2D::Texture2D()
  : m_format(TexFormat::TFORMAT_R8G8B8A8),
    m_height(50),
    m_isready(false),
    m_magfilter(TexFilter::TFILTER_BILINEAR),
    m_minfilter(TexFilter::TFILTER_BILINEAR),
    m_texture(0),
    m_width(50)
{
    glGenTextures(1, &this->m_texture);
    glBindTexture(GL_TEXTURE_2D, this->m_texture);

    assert(this->m_texture > 0);
}

//-----------------------------------------------------------------------------

unsigned Texture2D::GetHeight()
{
    return this->m_height;
}

//-----------------------------------------------------------------------------

GLuint Texture2D::GetID()
{
    return this->m_texture;
}

//-----------------------------------------------------------------------------

TexFilter Texture2D::GetMagFilter()
{
    return this->m_magfilter;
}

//-----------------------------------------------------------------------------

TexFilter Texture2D::GetMinFilter()
{
    return this->m_minfilter;
}

//-----------------------------------------------------------------------------

unsigned  Texture2D::GetWidth()
{
    return this->m_width;
}

//-----------------------------------------------------------------------------

void Texture2D::MakeCurrent()
{
    if (!this->m_isready)
    { this->reconf(); }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->m_texture);
}

//-----------------------------------------------------------------------------

void Texture2D::SetFormat(TexFormat f)
{
    this->m_format = f;
    this->m_isready = false;
}

//-----------------------------------------------------------------------------

void Texture2D::SetHeight(unsigned height)
{
    this->m_height = height;
    this->m_isready = false;
}

//-----------------------------------------------------------------------------

void Texture2D::SetMagFilter(TexFilter f)
{
    this->m_magfilter = f;
    this->m_isready = false;
}

//-----------------------------------------------------------------------------

void Texture2D::SetMinFilter(TexFilter f)
{
    this->m_minfilter = f;
    this->m_isready = false;
}

//-----------------------------------------------------------------------------

void Texture2D::SetWidth(unsigned width)
{
    this->m_width = width;
}

//-----------------------------------------------------------------------------

void Texture2D::reconf()
{
    glBindTexture(GL_TEXTURE_2D, this->m_texture);


    GLint mag = tfilter_map[(unsigned)this->m_magfilter];
    GLint min = tfilter_map[(unsigned)this->m_minfilter];
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    GLint fmt = tformat_map[(unsigned)this->m_format];
    glTexImage2D(GL_TEXTURE_2D, 0, fmt, this->m_width, this->m_height, 0, fmt, GL_UNSIGNED_BYTE, 0);
    dbg::check_gl();

    this->m_isready = true;
}