#include  "ParticleVisualizer.h"

ParticleVisualizer::ParticleVisualizer()
  : m_screen(480, 848),
    m_widget(*this)
{}

//-----------------------------------------------------------------------------

void ParticleVisualizer::Draw(Simulation& sim)
{
#ifndef GPU
    // Handle particle updates
    if (sim.WasReconfigured() || sim.WasUpdated())
    { this->load_vertices(sim); }
#else
    // Handle particle updates
    if (sim.WasReconfigured())
    { this->load_vertices(sim); }
#endif

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Handle view updates
    if (this->m_camera.WasUpdated())
    {
        this->m_camera.ResetUpdateFlag();
        this->render_impostor();
        this->update_uniforms();
    }

    // Render scene
    this->m_screen.MakeCurrent();
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    this->m_tex_target->GetTexture().MakeCurrent();
    this->m_billboards->Draw();
    this->m_coordinates->Draw();
}

//-----------------------------------------------------------------------------

Mesh& ParticleVisualizer::GetParticleMesh()
{
    return this->m_billboards->MeshData();
}

//-----------------------------------------------------------------------------

Widget* ParticleVisualizer::GetWidget()
{
    return &this->m_widget;
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::Init(Simulation& sim)
{
    this->m_camera.SetPosition(f3(0.0, 0.0, 65.0));
    this->m_camera.LookAt(f3::O);

    this->init_impostor();
    this->init_billboards();
    this->init_coords();

    this->load_vertices(sim);

    glClearDepth(1.0);
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::init_billboards()
{
    // Setup particle billboard shaders
    this->m_billboards = std::make_unique<RenderObject>(false);
    GPUProgram& gpudata = this->m_billboards->GPUData();
    gpudata.VertexShaderFile("../shaders/billboard.vs");
    gpudata.GeometryShaderFile("../shaders/billboard.gs");
    gpudata.PixelShaderFile("../shaders/billboard.ps");

    gpudata.SetUniform("size", uniform_t::UNIFORM_FLOAT, &this->m_widget.m_particle_size);

    std::string error = gpudata.GetError();
    if (error != "")
    {
        // TODO: report error
    }
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::init_coords()
{
    this->m_coordinates = std::make_unique<RenderObject>(false);
    
    GPUProgram& gpudata = this->m_coordinates->GPUData();
    gpudata.VertexShaderFile("../shaders/coord_colors.vs");
    gpudata.PixelShaderFile("../shaders/coord_colors.ps");

    attribute_t layout[] = {
        attribute_t::VERTEX_POSITION,
        attribute_t::VERTEX_END
    };

    float vertices[] = {
        0.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.0, 1.0
    };

    Mesh& meshdata = this->m_coordinates->MeshData();
    meshdata.SetTopology(Topology::TOPOLOGY_LINELIST);
    meshdata.SetVertexData(layout, 6, &vertices);

}

//-----------------------------------------------------------------------------

void ParticleVisualizer::init_impostor()
{
    // Setup frame buffer object
    this->m_tex_target = std::make_unique<TextureBuffer>(
        this->m_impostor_dim, 
        this->m_impostor_dim);

    // Setup impostor shaders/ uniforms
    this->m_impostor = std::make_unique<RenderObject>(false);
    GPUProgram& gpudata = this->m_impostor->GPUData();
    gpudata.VertexShaderFile("../shaders/impostor.vs");
    gpudata.GeometryShaderFile("../shaders/impostor.gs");
    gpudata.PixelShaderFile("../shaders/impostor.ps");

    float size = 128.0f;
    unsigned dim = this->m_impostor_dim;
    this->m_impostor->GPUData().SetUniform("desired_size", uniform_t::UNIFORM_FLOAT, &size);
    this->m_impostor->GPUData().SetUniform("dim", uniform_t::UNIFORM_UINT, &dim);

    // Set up vertex
    attribute_t layout[] = {
        attribute_t::VERTEX_POSITION,
        attribute_t::VERTEX_END
    };

    float impostor_pos[] = { 0.0, 0.0, 0.0 };

    unsigned indices[] = {0};

    this->m_impostor->MeshData().SetTopology(Topology::TOPOLOGY_POINTLIST);
    this->m_impostor->MeshData().SetVertexData(layout, 1, impostor_pos);

    std::string error = gpudata.GetError();
    if (error != "")
    {
        std::string msg = "GL error setting up impostor. Error is: ";
        msg += error;

        dbg::error(msg);
    }
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::load_vertices(Simulation& sim)
{
    attribute_t layout[] = {
        attribute_t::VERTEX_POSITION,
        attribute_t::VERTEX_END
    };

    unsigned long n_particles = sim.GetNParticles();
    float* vertices = new float[n_particles * 3];
    ParticleList& particles = sim.GetParticles();

    std::vector<float> ver;
    for (unsigned i = 0; i < n_particles; i++)
    {
        vertices[3 * i]     = particles[i].m_position.x;
        vertices[3 * i + 1] = particles[i].m_position.y;
        vertices[3 * i + 2] = particles[i].m_position.z;
    }

    Mesh& meshdata = this->m_billboards->MeshData();
    meshdata.SetVertexData(layout, n_particles, vertices);
    sim.ResetFlags();

    delete vertices;
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::render_impostor()
{
    GPUProgram& gpudata = this->m_billboards->GPUData();
    gpudata.SetUniform("size", uniform_t::UNIFORM_FLOAT, &this->m_widget.m_particle_size);

    this->m_tex_target->MakeCurrent();

    // Clear impostor texture to transparent
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    this->m_impostor->Draw();
}

//-----------------------------------------------------------------------------

void ParticleVisualizer::update_uniforms()
{
    f3 cam_pos = this->m_camera.GetPosition();
    float cam_posv[] = {cam_pos.x, cam_pos.y, cam_pos.z};
    this->m_billboards->GPUData().SetUniform("cam_pos", uniform_t::UNIFORM_VEC3, cam_posv);

    ff4 tmp_view = this->m_camera.GetTransform().inverse().transposed();
    float view[16] =
    {
        tmp_view.get(0, 0), tmp_view.get(0, 1), tmp_view.get(0, 2), tmp_view.get(0, 3),
        tmp_view.get(1, 0), tmp_view.get(1, 1), tmp_view.get(1, 2), tmp_view.get(1, 3),
        tmp_view.get(2, 0), tmp_view.get(2, 1), tmp_view.get(2, 2), tmp_view.get(2, 3),
        tmp_view.get(3, 0), tmp_view.get(3, 1), tmp_view.get(3, 2), tmp_view.get(3, 3)
    };

    ff4 tmp_proj = this->m_camera.GetProjection().transposed();
    float proj[16] =
    {
        tmp_proj.get(0, 0), tmp_proj.get(0, 1), tmp_proj.get(0, 2), tmp_proj.get(0, 3),
        tmp_proj.get(1, 0), tmp_proj.get(1, 1), tmp_proj.get(1, 2), tmp_proj.get(1, 3),
        tmp_proj.get(2, 0), tmp_proj.get(2, 1), tmp_proj.get(2, 2), tmp_proj.get(2, 3),
        tmp_proj.get(3, 0), tmp_proj.get(3, 1), tmp_proj.get(3, 2), tmp_proj.get(3, 3)
    };

    GPUProgram& gpudata1 = this->m_billboards->GPUData();
    gpudata1.SetUniform("vView", uniform_t::UNIFORM_4x4, view);
    gpudata1.SetUniform("vProjection", uniform_t::UNIFORM_4x4, proj);
    gpudata1.SetTextureSampler("tex_particle", this->m_tex_target->GetTexture(), 0);

    GPUProgram& gpudata2 = this->m_coordinates->GPUData();
    gpudata2.SetUniform("vView", uniform_t::UNIFORM_4x4, view);
    gpudata2.SetUniform("vProjection", uniform_t::UNIFORM_4x4, proj); 
}