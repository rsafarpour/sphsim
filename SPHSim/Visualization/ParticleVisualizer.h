
#ifndef VISUALIZATION_PARTICLEVISUALIZER
#define VISUALIZATION_PARTICLEVISUALIZER

#include <Dbg.h>
#include <RenderWindow.h>
#include <SDL.h>
#include <Simulation.h>

#include "Camera.h"
#include "GPUProgram.h"
#include "RenderObject.h"
#include "Screenbuffer.h"
#include "TextureBuffer.h"
#include "VisualizationWidget.h"

class ParticleVisualizer
{
    friend class VisualizationWidget;
public:
    ParticleVisualizer();

    void    Draw(Simulation& sim);
    Mesh&   GetParticleMesh();
    Widget* GetWidget();
    void    Init(Simulation& sim);

private:
    void init_billboards();
    void init_coords();
    void init_impostor();
    void load_vertices(Simulation& sim);
    void render_impostor();
    void update_uniforms();

    const unsigned m_impostor_dim = 128;

    std::unique_ptr<RenderObject>  m_billboards;
    Camera                         m_camera;
    std::unique_ptr<RenderObject>  m_coordinates;
    std::unique_ptr<RenderObject>  m_impostor;
    Screenbuffer                   m_screen;
    std::unique_ptr<TextureBuffer> m_tex_target;
    VisualizationWidget            m_widget;
};

#endif