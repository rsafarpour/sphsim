
#ifndef VISUALIZATION_RENDEROBJECT
#define VISUALIZATION_RENDEROBJECT

#include "GPUProgram.h"
#include "Mesh.h"

class RenderObject
{
public:
    RenderObject(bool instanced);

    void         Draw();
    GPUProgram&  GPUData();
    void         MakeCurrent();
    Mesh&        MeshData();

private:
    bool        m_instanced;
    GPUProgram  m_program;
    Mesh        m_mesh;

};

#endif