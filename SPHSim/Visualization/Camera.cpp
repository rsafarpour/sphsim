
#include "Camera.h"

Camera::Camera()
    : m_aspect(1.77777777778f), // = 16:9
    m_cached(false),
    m_far(100.0f),
    m_fovy(PI / 4.0f),
    m_near(5.0f),
    m_updated(true),
    m_position(f3::O),
    m_direction(-f3::UNIT_Z)
{}

//-------------------------------------------------------------------------

f3 Camera::GetPosition()
{
    return this->m_position;
}

//-------------------------------------------------------------------------

ff4 Camera::GetProjection()
{
    ff4 proj;
    float fovh = 2 * atan(tan(this->m_fovy / 2) * this->m_aspect);

    float fovx = 1 / (tan(0.5f*fovh));
    float fovy = 1 / (tan(0.5f*this->m_fovy));
    float nf1 = -(this->m_far / (this->m_far - this->m_near));
    float nf2 = -2 * (this->m_near * this->m_far) / (this->m_far - this->m_near);

    proj.set(0, 0, fovx);
    proj.set(1, 1, fovy);
    proj.set(2, 2, nf1);
    proj.set(2, 3, nf2);
    proj.set(3, 2, -1.0f);
    return proj;
}

//-------------------------------------------------------------------------

ff4 Camera::GetTransform()
{
    if (this->m_direction == -f3::UNIT_Z)
    {
        ff4 r = ff4::I;
        r.set(0, 3, this->m_position.x);
        r.set(1, 3, this->m_position.y);
        r.set(2, 3, this->m_position.z);
        return r;
    }

    float cos = this->m_direction.dot(-f3::UNIT_Z) / this->m_direction.norm();

    float angle = -acos(cos);
    f3 axis = this->m_direction.cross(-f3::UNIT_Z);

    quat rot = quat::FromRotation(angle, axis);
    ff4 r    = rot.matrix();
    r.set(0, 3, this->m_position.x);
    r.set(1, 3, this->m_position.y);
    r.set(2, 3, this->m_position.z);
    return r;
}

//-------------------------------------------------------------------------

ff4 Camera::GetVP()
{
    if (this->m_cached)
    { return this->m_vp; }

    ff4 view = this->GetTransform().inverse();
    this->m_vp = this->GetProjection() * view;
    return this->m_vp;
}

//-------------------------------------------------------------------------

void Camera::LookAt(const f3& position)
{
    f3 direction = this->m_position.to(position);
    this->SetDirection(direction);
}

//-------------------------------------------------------------------------

void Camera::ResetUpdateFlag()
{
    this->m_updated = false;
}

//-------------------------------------------------------------------------

void Camera::SetPosition(f3& position)
{
    if (position != this->m_position)
    {
        this->m_updated = true;
        this->m_position = position;
    }
}

//-------------------------------------------------------------------------

void Camera::SetDirection(f3& direction)
{
    direction.normalize();
    if (direction != f3::O && direction != this->m_direction)
    {
        this->m_updated = true;
        this->m_direction = direction;
    }
}

//-------------------------------------------------------------------------

bool Camera::WasUpdated()
{
    return (this->m_updated);
}