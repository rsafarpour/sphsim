
#include "Shader.h"

Shader::Shader()
  : m_iscompiled(false),
    m_compiled(0)
{}

//-----------------------------------------------------------------------------

void Shader::Clear()
{
    if (this->m_iscompiled)
    {
        glDeleteShader(this->m_compiled);
        this->m_compiled = 0;
        this->m_iscompiled = false;
    }
}

//-----------------------------------------------------------------------------

GLuint Shader::GetID()
{
    return this->m_compiled;
}

//-----------------------------------------------------------------------------

void Shader::SetSource(std::string src)
{
    this->m_code = src;
}

//-----------------------------------------------------------------------------

void Shader::SourceFromFile(std::string file)
{
    std::ifstream infile(file, std::ios::in);

    this->m_code = std::string((std::istreambuf_iterator<char>(infile)),
        std::istreambuf_iterator<char>());
}

//-----------------------------------------------------------------------------

void Shader::Compile()
{
    if (!this->m_iscompiled)
    { 
        this->compile_impl(); 

        GLint compile_succeeded;
        glGetShaderiv(this->m_compiled, GL_COMPILE_STATUS, &compile_succeeded);
        if (!compile_succeeded)
        {
            // TODO: log error

            int l = 0;
            glGetShaderiv(this->m_compiled, GL_INFO_LOG_LENGTH, &l);

            char* buf = new char[l];
            glGetShaderInfoLog(this->m_compiled, l, &l, buf);
            std::string err = buf;
            delete[] buf;

            OutputDebugString(err.c_str());
        }
    }
}

//=============================================================================

void VertexShader::compile_impl()
{
    const char* in[] = {this->m_code.c_str()};

    this->m_compiled = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this->m_compiled, 1, in, 0);
    glCompileShader(this->m_compiled);
}

//=============================================================================

void GeometryShader::compile_impl()
{
    const char* in[] = {this->m_code.c_str()};

    this->m_compiled = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(this->m_compiled, 1, in, 0);
    glCompileShader(this->m_compiled);
}

//=============================================================================

void PixelShader::compile_impl()
{ 
    const char* in[] = {this->m_code.c_str()};

    this->m_compiled = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->m_compiled, 1, in, 0);
    glCompileShader(this->m_compiled);
}