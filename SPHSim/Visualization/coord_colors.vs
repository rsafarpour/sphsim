#version 450

uniform mat4 vView;
uniform mat4 vProjection;
in vec3 position;
out vec3 color;

void main(void) 
{
	mat4 vp = vProjection * vView;
	
	if(gl_VertexID < 2) 
	{color = vec3(1.0, 0.0, 0.0);}
	else if(gl_VertexID < 4)
	{color = vec3(0.0, 1.0, 0.0);}
	else
	{color = vec3(0.0, 0.0, 1.0);}
	
	gl_Position = vp * vec4(position, 1.0);


}