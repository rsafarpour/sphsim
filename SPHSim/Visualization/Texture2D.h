
#ifndef VISUALIZATION_TEXTURE2D
#define VISUALIZATION_TEXTURE2D

#include <assert.h>
#include <Dbg.h>
#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

enum class TexFilter : unsigned
{
    TFILTER_NEAREST = 0,
    TFILTER_BILINEAR,
    TFILTER_TRILINEAR
};

enum class TexFormat : unsigned
{
    TFORMAT_DEPTH16 = 0,
    TFORMAT_DEPTH24,
    TFORMAT_DEPTH32,
    TFORMAT_R8G8B8A8
};

static const GLint tfilter_map[] = {
    GL_NEAREST,              // TFILTER_NEAREST
    GL_LINEAR,               // TFILTER_BILINEAR
    GL_LINEAR_MIPMAP_LINEAR, // TFILTER_TRILINEAR
};

static const GLint tformat_map[] = {
    GL_DEPTH_COMPONENT16, // TFORMAT_DEPTH16
    GL_DEPTH_COMPONENT24, // TFORMAT_DEPTH24
    GL_DEPTH_COMPONENT32, // TFORMAT_DEPTH32
    GL_RGBA               // TFORMAT_R8G8B8A8
};

class Texture2D
{
public:
    Texture2D();

    unsigned  GetHeight();
    GLuint    GetID();
    TexFilter GetMagFilter();
    TexFilter GetMinFilter();
    unsigned  GetWidth();
    void      MakeCurrent();
    void      SetFormat(TexFormat f);
    void      SetHeight(unsigned height);
    void      SetMagFilter(TexFilter f);
    void      SetMinFilter(TexFilter f);
    void      SetWidth(unsigned width);

private:
    void reconf();

    TexFormat m_format;
    unsigned  m_height;
    bool      m_isready;
    TexFilter m_magfilter;
    TexFilter m_minfilter;
    GLuint    m_texture;
    unsigned  m_width;
};

#endif