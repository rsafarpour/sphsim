#version 450

uniform mat4 vView;
uniform mat4 vProjection;
in vec3 position;

void main(void) 
{
	gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}