
#include "GPUProgram.h"

GPUProgram::GPUProgram()
  : m_hasvs(false),
    m_hasgs(false),
    m_hasps(false),
    m_islinked(false)
{}

void GPUProgram::DisableAttribute(const char* name)
{
    GLint location = this->attrib_location(name);

    if (location >= 0)
    { glDisableVertexAttribArray(location); }
    
}

//-----------------------------------------------------------------------------

void GPUProgram::EnableAttribute(const char* name)
{
    GLint location = this->attrib_location(name);

    if (location >= 0)
    { glEnableVertexAttribArray(location); }
}

//-----------------------------------------------------------------------------

void GPUProgram::GeometryShaderFile(const char* path)
{
    this->addingshader();
    this->m_gshader.SourceFromFile(path);
    this->m_hasgs = true;
}

//-----------------------------------------------------------------------------

std::string GPUProgram::GetError()
{
    GLenum error = glGetError();

    if (error == GL_NO_ERROR)
    { return ""; }
    else
    {
        return std::string((char*)gluErrorString(error));
    }
}

//-----------------------------------------------------------------------------

void GPUProgram::Link()
{
    if (!this->m_hasvs || !this->m_hasps)
    { 
        dbg::error("GPU program misses vertex or pixel shader!"); 
        return;
    }

    if (!this->m_islinked)
    {
        this->m_program = glCreateProgram();

        if (this->m_hasvs) { this->attach(&m_vshader); }
        if (this->m_hasgs) { this->attach(&m_gshader); }
        if (this->m_hasps) { this->attach(&m_pshader); }

        glLinkProgram(this->m_program);

        GLint link_succeeded;
        glGetProgramiv(this->m_program, GL_LINK_STATUS, &link_succeeded);
        if (!link_succeeded)
        {
            // TODO: log error

            GLint l = 0;
            glGetProgramiv(this->m_program, GL_INFO_LOG_LENGTH, &l);

            char* buf = new char[l];
            glGetProgramInfoLog(this->m_program, l, &l, buf);
            std::string err = buf;
            delete[] buf;

            OutputDebugString(err.c_str());
        }

        this->m_islinked = true;

        if (this->m_hasvs) { this->detach(&m_vshader); }
        if (this->m_hasgs) { this->detach(&m_gshader); }
        if (this->m_hasps) { this->detach(&m_pshader); }
    }
}

//-----------------------------------------------------------------------------

void GPUProgram::MakeCurrent()
{
    if (!this->m_islinked)
    { this->Link(); }

    glUseProgram(this->m_program);

    for (auto iter : this->m_attributes)
    { glEnableVertexAttribArray(iter.second); }
}

//-----------------------------------------------------------------------------

void GPUProgram::PixelShaderFile(const char* path)
{
    this->addingshader();
    this->m_pshader.SourceFromFile(path);
    this->m_hasps = true;
}

//-----------------------------------------------------------------------------

void GPUProgram::SetAttribute(const char* name, unsigned components, unsigned bytes, void* data)
{
    if (!this->m_islinked)
    { this->Link(); }

    GLint location = this->attrib_location(name);

    if (location >= 0)
    { 
        glBufferData(GL_ARRAY_BUFFER, bytes, data, GL_STREAM_DRAW);
        glVertexAttribPointer(location, components, GL_FLOAT, GL_FALSE, 0, 0);
    }
}

//-----------------------------------------------------------------------------

void GPUProgram::SetTextureSampler(const char* name, Texture2D& tex, unsigned unit)
{
    
    this->MakeCurrent();

    GLint location = glGetUniformLocation(this->m_program, name);
    
    if (location >= 0)
    {
        //tex.MakeCurrent();
        glUniform1i(location, unit);
    }
    else
    { 
        std::string msg = "Location of uniform \""; 
        msg += name;
        msg += "\" not found!";
        dbg::warning(msg.c_str());
    }
}

//-----------------------------------------------------------------------------

void GPUProgram::SetUniform(const char* name, uniform_t type, void* data)
{  
    this->MakeCurrent();
    
    GLint location = glGetUniformLocation(this->m_program, name);
    if (location == -1)
    {
        // TODO: handle error
        return;
    }

    switch (type)
    {
    case uniform_t::UNIFORM_FLOAT:  glUniform1f(location, *((GLfloat*)data));                    break;
    case uniform_t::UNIFORM_UINT:   glUniform1ui(location, *((unsigned*)data));                  break;
    case uniform_t::UNIFORM_INT:    glUniform1i(location, *((unsigned*)data));                   break;
    case uniform_t::UNIFORM_VEC2:   glUniform2fv(location, 1, (GLfloat*)data);                   break;
    case uniform_t::UNIFORM_VEC3:   glUniform3fv(location, 1, (GLfloat*)data);                   break;
    case uniform_t::UNIFORM_VEC4:   glUniform4fv(location, 1, (GLfloat*)data);                   break;
    case uniform_t::UNIFORM_2x3:    glUniformMatrix2x3fv(location, 1, GL_FALSE, (GLfloat*)data); break;
    case uniform_t::UNIFORM_3x4:    glUniformMatrix3x4fv(location, 1, GL_FALSE, (GLfloat*)data); break;
    case uniform_t::UNIFORM_4x4:    glUniformMatrix4fv(location, 1, GL_FALSE, (GLfloat*)data);   break;
    }
}

//-----------------------------------------------------------------------------

void GPUProgram::VertexShaderFile(const char* path)
{
    this->addingshader();
    this->m_vshader.SourceFromFile(path);
    this->m_hasvs = true;
}

//-----------------------------------------------------------------------------

void GPUProgram::addingshader()
{
    if (this->m_islinked)
    { glDeleteProgram(this->m_program); }

    this->m_islinked = false;
}

//-----------------------------------------------------------------------------

void GPUProgram::attach(Shader* s)
{
    s->Compile();
    glAttachShader(this->m_program, s->GetID());
}

//-----------------------------------------------------------------------------

GLuint GPUProgram::attrib_location(const char* name)
{
    GLint attrib = -1;
    attrib = glGetAttribLocation(this->m_program, name);

    if (attrib < 0)
    {
        std::string msg = "Failed determining location of attribute \"";
        msg += name;
        msg += "\" in GPU program";

        dbg::error(msg);
    }

    return attrib;
}

//-----------------------------------------------------------------------------

void GPUProgram::detach(Shader* s)
{
    glDetachShader(this->m_program, s->GetID());
    s->Clear();
}