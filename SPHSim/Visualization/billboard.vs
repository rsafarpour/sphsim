#version 450

uniform mat4 vView;
uniform mat4 vProjection;
in vec3 position;

out mat4 gView;
out mat4 gProjection;

void main(void) 
{
	gl_Position = vec4(position, 1.0); 
	gProjection = vProjection;
	gView = vView;
}