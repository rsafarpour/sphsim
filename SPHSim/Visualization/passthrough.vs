#version 450

uniform mat4 vView;
uniform mat4 vProjection;
in vec3 position;

void main(void) 
{
	mat4 vp = vProjection * vView;
	gl_Position = vp * vec4(position, 1.0);
}