
#ifndef VISUALIZATION_VISUALIZATIONWIDGET
#define VISUALIZATION_VISUALIZATIONWIDGET

#include <Widget.h>
#include <imgui.h>

#include "Properties.h"

class Visualization;
class VisualizationWidget : public Widget
{
    friend class ParticleVisualizer;
public:
    VisualizationWidget(ParticleVisualizer& vis);

    virtual void Definition() override;

private:
    float               m_particle_size;
    ParticleVisualizer& m_vis;
};

#endif