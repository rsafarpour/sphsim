
#ifndef VISUALIZATION_TEXTUREBUFFER
#define VISUALIZATION_TEXTUREBUFFER

#include <assert.h>
#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

#include "Framebuffer.h"
#include "Texture2D.h"

class TextureBuffer : public Framebuffer
{
public:
    TextureBuffer(unsigned width, unsigned height);
    ~TextureBuffer();

    Texture2D& GetTexture();
    virtual void MakeCurrent();

private:

    void setup();

    bool       m_isready;
    GLuint     m_depth;
    GLuint     m_object;
    Texture2D  m_target;
};

#endif