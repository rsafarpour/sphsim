
#ifndef VISUALIZATION_VERTEXARRAY
#define VISUALIZATION_VERTEXARRAY

#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>
#include <cuda_gl_interop.h>
#include <CudaDbg.h>

#include "GPUProgram.h"
#include "Topology.h"

enum class attribute_t : unsigned
{
    VERTEX_POSITION = 0,
    VERTEX_NORMAL   = 1,
    VERTEX_COLOR    = 2,
    VERTEX_UV       = 3,
    VERTEX_END
};

static const unsigned attr_sizes[] = 
{
    sizeof(float) * 3, // = attribute_t::VERTEX_POSITION
    sizeof(float) * 3, // = attribute_t::VERTEX_NORMAL
    sizeof(float) * 3, // = attribute_t::VERTEX_COLOR
    sizeof(float) * 2  // = attribute_t::VERTEX_UV
};

//=============================================================================

class Mesh
{
    friend class RederObject;
public:
    Mesh(GPUProgram* program);
    ~Mesh();

    void    CudaMakeObject();
    float3* CudaMap();
    void    CudaUnmap();
    void    Draw();
    void    MakeCurrent();
    void    SetVertexData(attribute_t* layout, unsigned n, void* data);
    void    SetInstanced(bool on);
    void    SetTopology(Topology topo);

private:
    unsigned layout_size(attribute_t* layout);

    bool                  m_cudaregistered;
    unsigned long         m_n_vertices;
    GPUProgram*           m_program;
    cudaGraphicsResource* m_cudaresource;
    Topology              m_topology;
    GLuint                m_vbo;
    GLuint                m_vao;
};

#endif