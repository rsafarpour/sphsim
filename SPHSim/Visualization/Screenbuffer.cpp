
#include "Screenbuffer.h"

Screenbuffer::Screenbuffer(unsigned height, unsigned width)
{
    this->m_screenx = width;
    this->m_screeny = height;
}

//-----------------------------------------------------------------------------

void Screenbuffer::Resize(unsigned height, unsigned width)
{
    this->m_screenx = width;
    this->m_screeny = height;
}

//-----------------------------------------------------------------------------

void Screenbuffer::MakeCurrent()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, this->m_screenx, this->m_screeny);
}