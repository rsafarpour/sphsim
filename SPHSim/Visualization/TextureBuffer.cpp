
#include "TextureBuffer.h"

TextureBuffer::TextureBuffer(unsigned width, unsigned height)
  : m_isready(false),
    m_object(0)
{
    this->m_target.SetMagFilter(TexFilter::TFILTER_BILINEAR);
    this->m_target.SetMinFilter(TexFilter::TFILTER_BILINEAR);
    this->m_target.SetFormat(TexFormat::TFORMAT_R8G8B8A8);
    this->m_target.SetHeight(height);
    this->m_target.SetWidth(width);
    this->m_target.MakeCurrent();
}

//-----------------------------------------------------------------------------

TextureBuffer::~TextureBuffer()
{
    glDeleteFramebuffers(1, &this->m_object);
}

//-----------------------------------------------------------------------------

Texture2D& TextureBuffer::GetTexture()
{
    return this->m_target;
}

//-----------------------------------------------------------------------------

void TextureBuffer::MakeCurrent()
{
    // Make sure the texture is set up correctly
    if (!this->m_isready)
    { this->setup(); }

    glBindFramebuffer(GL_FRAMEBUFFER, this->m_object);
    glViewport(0, 0, this->m_target.GetWidth(), this->m_target.GetHeight());
}

//-----------------------------------------------------------------------------

void TextureBuffer::setup()
{
    glGenFramebuffers(1, &this->m_object);
    glBindFramebuffer(GL_FRAMEBUFFER, this->m_object);

    glBindTexture(GL_TEXTURE_2D, this->m_target.GetID());

    // Append color texture to framebuffer
    glFramebufferTexture2D(
        GL_FRAMEBUFFER,
        GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_2D,
        this->m_target.GetID(),
        0);

    // Append depth buffer to framebuffer
    glGenRenderbuffers(1, &this->m_depth);
    glBindRenderbuffer(GL_RENDERBUFFER, this->m_depth);
    glRenderbufferStorage(GL_RENDERBUFFER, 
        GL_DEPTH_COMPONENT, 
        this->m_target.GetWidth(), 
        this->m_target.GetHeight());

    glFramebufferRenderbuffer(
        GL_FRAMEBUFFER, 
        GL_DEPTH_ATTACHMENT, 
        GL_RENDERBUFFER, 
        this->m_depth);


    GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, DrawBuffers);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        // TODO: error handling
        assert(false);
    }

    this->m_isready = true;
}