
#include "Mesh.h"

Mesh::Mesh(GPUProgram* program)
  : m_cudaregistered(false),
    m_n_vertices(0),
    m_vbo(0),
    m_program(program),
    m_topology(Topology::TOPOLOGY_POINTLIST)
{
    glGenVertexArrays(1, &this->m_vao);
    glGenBuffers(1, &this->m_vbo);
}

//-----------------------------------------------------------------------------

Mesh::~Mesh()
{
    glDeleteBuffers(1, &this->m_vbo);
    glDeleteVertexArrays(1, &this->m_vao);
    this->m_vbo     = 0;

    if (this->m_cudaregistered)
    { cudaGraphicsUnregisterResource(this->m_cudaresource); }

}

//-----------------------------------------------------------------------------

void  Mesh::CudaMakeObject()
{
    // Makes sure that registration occurs only once
    if (!this->m_cudaregistered)
    {
        check(cudaGraphicsGLRegisterBuffer(&this->m_cudaresource, this->m_vbo, cudaGraphicsMapFlagsNone));
        this->m_cudaregistered = true;
    }
}

//-----------------------------------------------------------------------------

float3* Mesh::CudaMap()
{
    // VBO must be registered before mapping
    if (!this->m_cudaregistered)
    { this->CudaMakeObject(); }

    void* buffer;
    size_t n_bytes;
    check(cudaGraphicsMapResources(1, &this->m_cudaresource));
    check(cudaGraphicsResourceGetMappedPointer(&buffer, &n_bytes, this->m_cudaresource));
    return static_cast<float3*>(buffer);
}

//-----------------------------------------------------------------------------

void Mesh::CudaUnmap()
{
    check(cudaGraphicsUnmapResources(1, &this->m_cudaresource));
}

//-----------------------------------------------------------------------------

void Mesh::Draw()
{
    this->m_program->EnableAttribute("position");

    GLenum mode = topology_map[(unsigned)this->m_topology];
    glDrawArrays(mode, 0, this->m_n_vertices);

    this->m_program->DisableAttribute("position");
}

//-----------------------------------------------------------------------------

void Mesh::MakeCurrent()
{
    glBindVertexArray(this->m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->m_vbo);
}

//-----------------------------------------------------------------------------

void Mesh::SetVertexData(attribute_t* layout, unsigned n, void* data)
{
    this->m_n_vertices = n;

    this->MakeCurrent();

    unsigned size = this->layout_size(layout);
    this->m_program->SetAttribute("position", 3, n*size, data);
}

//-----------------------------------------------------------------------------

void Mesh::SetTopology(Topology topo)
{
    this->m_topology = topo;
}

//-----------------------------------------------------------------------------

unsigned Mesh::layout_size(attribute_t* layout)
{
    unsigned i    = 0;
    unsigned size = 0;
    while (layout[i] != attribute_t::VERTEX_END)
    {
        size += attr_sizes[(unsigned)layout[i]];
        i++;
    }

    return size;
}