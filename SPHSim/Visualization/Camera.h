
#ifndef VISUALIZATION_CAMERA
#define VISUALIZATION_CAMERA

#include <constants.h>
#include <f3.h>
#include <ff4.h>
#include <quat.h>

class Camera
{
public:
    Camera();

    f3   GetPosition();
    ff4   GetProjection();
    ff4   GetTransform();
    ff4   GetVP();
    void LookAt(const f3& position);
    void ResetUpdateFlag();
    void SetPosition(f3& position);
    void SetDirection(f3& direction);
    bool WasUpdated();

private:
    quat rotation_();

    float m_aspect;
    bool  m_cached;
    f3    m_direction;
    float m_far;
    float m_fovy;
    float m_near;
    bool  m_updated;
    f3    m_position;
    ff4   m_vp;
};

#endif