
#ifndef UI_UI
#define UI_UI

#include <SDL.h>
#include <vector>
#include <memory>

#include "Widget.h"

class UI
{
public:
    void AddWidget(Widget* w);
    void Draw();
    void Initialize(unsigned win_x, unsigned win_y);
    bool Processed(SDL_Event& evt);
    void ResizedWindow(unsigned win_x, unsigned win_y);
    void Terminate();

private:
    std::vector<std::unique_ptr<Widget>> widgets;
};

#endif