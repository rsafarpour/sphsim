
#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

#include "imgui.h"
#include "UI.h"
#include "utils.h"

void UI::AddWidget(Widget* w)
{
    this->widgets.push_back(std::unique_ptr<Widget>(w));
}

//-----------------------------------------------------------------------------

void UI::Draw()
{
    ImGui::NewFrame();

    for (auto& w : this->widgets)
    { w->Definition(); }

    ImGui::Render();
}

//-----------------------------------------------------------------------------

void UI::Initialize(unsigned win_x, unsigned win_y)
{
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize.x = static_cast<float>(win_x);
    io.DisplaySize.y = static_cast<float>(win_y);
    io.RenderDrawListsFn = ImGui::renderfn;

    ImGui::setup_imgui();
}

//-----------------------------------------------------------------------------

bool UI::Processed(SDL_Event& evt)
{
    ImGuiIO& io = ImGui::GetIO();
    int x, y;

    SDL_GetMouseState(&x, &y);
    io.MousePos.x = static_cast<float>(x);
    io.MousePos.y = static_cast<float>(y);

    switch (evt.type)
    {
    case SDL_MOUSEWHEEL:
    {
        if (evt.wheel.y > 0)
            io.MouseWheel = 1;
        if (evt.wheel.y < 0)
            io.MouseWheel = -1;
        return true;
    }
    case SDL_MOUSEBUTTONDOWN:
    {
        if (evt.button.button == SDL_BUTTON_LEFT)
        { io.MouseDown[0] = true; }
        if (evt.button.button == SDL_BUTTON_RIGHT)
        { io.MouseDown[1] = true; }
        if (evt.button.button == SDL_BUTTON_MIDDLE)
        { io.MouseDown[2] = true; }
        return true;
    }
    case SDL_MOUSEBUTTONUP:
    {
        if (evt.button.button == SDL_BUTTON_LEFT)
        { io.MouseDown[0] = false; }
        if (evt.button.button == SDL_BUTTON_RIGHT)
        { io.MouseDown[1] = false; }
        if (evt.button.button == SDL_BUTTON_MIDDLE)
        { io.MouseDown[2] = false; }
    }
    case SDL_TEXTINPUT:
    {
        io.AddInputCharactersUTF8(evt.text.text);
        return true;
    }
    case SDL_KEYDOWN:
    case SDL_KEYUP:
    {
        int key = evt.key.keysym.sym & ~SDLK_SCANCODE_MASK;
        io.KeysDown[key] = (evt.type == SDL_KEYDOWN);
        io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
        io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
        io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
        io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
        return true;
    }

    default:
        return false;
    }
}

//-----------------------------------------------------------------------------

void UI::ResizedWindow(unsigned win_x, unsigned win_y)
{
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize.x = static_cast<float>(win_x);
    io.DisplaySize.y = static_cast<float>(win_y);
}

//-----------------------------------------------------------------------------

void UI::Terminate()
{ 
    ImGui::shutdown_imgui();
}