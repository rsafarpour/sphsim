
#ifndef UI_UTILS
#define UI_UTILS

#include "imgui.h"

namespace ImGui
{
    void renderfn(ImDrawData* data);
    void setup_fonts();
    void setup_imgui();
    void shutdown_imgui();
}

#endif