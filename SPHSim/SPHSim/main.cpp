#include <glew.h>
#include <wglext.h>
#include <glext.h>
#include <gl/GL.h>

#include <UI.h>
#include <Widget.h>
#include <SDL.h>

#include <MainLoop.h>
#include <imgui.h>
#include <ParticleVisualizer.h>
#include <Simulation.h>

#include "SPHSim.h"

#include <cuda_profiler_api.h>

int main(int argc, char* argv[])
{
    MainLoop loop(new SPHSim);
    RenderWindow& w = loop.GetWindow();
    w.SetWidth(848);
    w.SetHeight(480);
    w.SetTitle("Output Window");
    
    loop.Init();
    loop.Run();

    cudaProfilerStop();

    return 0;
}