
#include "SPHSim.h"

void SPHSim::Initialize(const RenderWindow& wnd)
{
    using std::chrono::steady_clock;

    m_visualization.Init(this->m_simulation);
    m_ui.Initialize(wnd.GetWidth(), wnd.GetHeight());

    this->m_ui.AddWidget(this->m_simulation.GetWidget());
    this->m_ui.AddWidget(this->m_visualization.GetWidget());

    this->m_t_last_update = steady_clock::now();
}

//-----------------------------------------------------------------------------

void SPHSim::Draw()
{
    using std::chrono::duration_cast;
    using std::chrono::system_clock;

    // Get time passed since last simulation update
    tpoint_t now = system_clock::now();
    auto time_passed = duration_cast<ms_t>(now - this->m_t_last_update).count();
    this->m_t_last_update = now;

#ifdef GPU
    // Necessary because in gpu mode the VBO is used by
    // CUDA kernels in Simulation class
    if (this->m_simulation.WasReconfigured())
    { this->m_visualization.Draw(m_simulation); }
#endif

    // Advance simulation as far as possible
    Mesh& particles = this->m_visualization.GetParticleMesh();
    this->m_simulation.Update(particles, time_passed);

    // Visualize particles
    m_visualization.Draw(m_simulation);
    m_ui.Draw();
}

//-----------------------------------------------------------------------------

bool SPHSim::Processed(SDL_Event& evt)
{ return m_ui.Processed(evt); }

//-----------------------------------------------------------------------------

void SPHSim::Terminate()
{ m_ui.Terminate(); }