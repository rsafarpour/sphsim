
#ifndef SPHSIM_CONTENT
#define SPHSIM_CONTENT

#include <chrono>
#include <Content.h>
#include <ParticleVisualizer.h>
#include <Simulation.h>
#include <UI.h>

typedef std::chrono::system_clock::time_point tpoint_t;
typedef std::chrono::milliseconds ms_t;

class SPHSim : public Content
{
public:

    virtual void Initialize(const RenderWindow& wnd) override;
    virtual void Draw()                              override;
    virtual bool Processed(SDL_Event& evt)           override;
    virtual void Terminate()                         override;
  
private:
    Simulation         m_simulation;
    tpoint_t           m_t_last_update;
    UI                 m_ui;
    ParticleVisualizer m_visualization;
};

#endif