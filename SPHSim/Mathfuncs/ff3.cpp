
#include "ff3.h"

const ff3 ff3::I(1.0f, 0.0f, 0.0f,
                 0.0f, 1.0f, 0.0f,
                 0.0f, 0.0f, 1.0f);

//------------------------------------------------------------------------------

ff3::ff3()
{
    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff3::m_dimension; j++)
        { this->m_values[i][j] = 0.0f; }
    }
}

//------------------------------------------------------------------------------

ff3::ff3(
    float _00, float _01, float _02,
    float _10, float _11, float _12,
    float _20, float _21, float _22)
{
    this->m_values[0][0] = _00;
    this->m_values[0][1] = _01;
    this->m_values[0][2] = _02;
    this->m_values[1][0] = _10;
    this->m_values[1][1] = _11;
    this->m_values[1][2] = _12;
    this->m_values[2][0] = _20;
    this->m_values[2][1] = _21;
    this->m_values[2][2] = _22;
}

//------------------------------------------------------------------------------

ff3::ff3(f3 r0, f3 r1, f3 r2)
    : ff3(r0.x, r0.y, r0.z,
               r1.x, r1.y, r1.z, 
               r2.x, r2.y, r2.z)
{}

//------------------------------------------------------------------------------

void ff3::set(unsigned row, unsigned col, float value)
{
    if (row >= ff3::m_dimension)
    { return; }

    if (col >= ff3::m_dimension)
    { return; }

    this->m_values[row][col] = value;
}

//------------------------------------------------------------------------------

float ff3::get(unsigned row, unsigned col)
{
    if (row >= ff3::m_dimension)
    { return (float)nan(""); }

    if (col >= ff3::m_dimension)
    { return (float)nan(""); }

    return this->m_values[row][col];
}

//------------------------------------------------------------------------------

void ff3::times(float factor)
{
    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff3::m_dimension; j++)
        { this->m_values[i][j] *= factor; }
    }
}

//------------------------------------------------------------------------------

f3 ff3::row(unsigned char row)
{
    if (row >= ff3::m_dimension)
    { return f3((float)nan(""), (float)nan(""), (float)nan("")); }

    f3 r;
    r.x = this->m_values[row][0];
    r.y = this->m_values[row][1];
    r.z = this->m_values[row][2];
    return r;
}

//------------------------------------------------------------------------------

f3 ff3::col(unsigned char col)
{
    if (col >= ff3::m_dimension)
    { return f3((float)nan(""), (float)nan(""), (float)nan("")); }

    f3 r;
    r.x = this->m_values[0][col];
    r.y = this->m_values[1][col];
    r.z = this->m_values[2][col];
    return r;
}

//------------------------------------------------------------------------------

float ff3::det()
{
    float r = 0;
    r =  m_values[0][0] * m_values[1][1] * m_values[2][2];
    r += m_values[0][1] * m_values[1][2] * m_values[2][0];
    r += m_values[0][2] * m_values[1][0] * m_values[2][1];
    r -= m_values[0][2] * m_values[1][1] * m_values[2][0];
    r -= m_values[0][1] * m_values[1][0] * m_values[2][2];
    r -= m_values[0][0] * m_values[1][2] * m_values[2][1];
    return r;
}

//------------------------------------------------------------------------------

ff3 ff3::transposed()
{
    ff3 r(*this);
    r.transpose();
    return r;
}

//------------------------------------------------------------------------------

void ff3::transpose()
{
    float temp[ff3::m_dimension][ff3::m_dimension];
    for (unsigned i = 0; i < ff3::m_dimension; i++)
    {
        for (unsigned j = 0; j < ff3::m_dimension; j++)
        { temp[j][i] = this->m_values[i][j]; }
    }

    std::swap(this->m_values, temp);
}

//------------------------------------------------------------------------------

ff3 ff3::adjugated()
{
    ff3 r(*this);
    r.adjugate();
    return r;
}

//------------------------------------------------------------------------------

void ff3::adjugate()
{
    ff3 r;
    float a = this->m_values[0][0];
    float b = this->m_values[0][1];
    float c = this->m_values[0][2];
    float d = this->m_values[1][0];
    float e = this->m_values[1][1];
    float f = this->m_values[1][2];
    float g = this->m_values[2][0];
    float h = this->m_values[2][1];
    float i = this->m_values[2][2];

    float temp[ff3::m_dimension][ff3::m_dimension];
    temp[0][0] = e*i - f*h;
    temp[0][1] = c*h - b*i;
    temp[0][2] = b*f - c*e;
    temp[1][0] = f*g - d*i;
    temp[1][1] = a*i - c*g;
    temp[1][2] = c*d - a*f;
    temp[2][0] = d*h - e*g;
    temp[2][2] = a*e - b*d;

    std::swap(this->m_values, temp);
}

//------------------------------------------------------------------------------

ff3 ff3::inverse()
{
    ff3 r(*this);
    r.invert();
    return r;
}

//------------------------------------------------------------------------------

void ff3::invert()
{
    float det = this->det();

    if (equal(det, 0.0f))
    { return; }

    this->adjugate();
    this->times(1 / det);
}

//------------------------------------------------------------------------------

ff3 ff3::operator+(ff3& m)
{
    ff3 r;

    for (int i = 0; i < ff3::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff3::m_dimension; j++)
        { r.set(i, j, this->m_values[i][j] + m.get(i, j)); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff3 ff3::operator-(ff3& m)
{
    ff3 r; ff3;
    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (int j = 0; j < ff3::m_dimension; j++)
        { r.set(i, j, this->m_values[i][j] + m.get(i, j)); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff3 ff3::operator-()
{
    ff3 r;
    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (int j = 0; j < ff3::m_dimension; j++)
        { r.set(i, j, -this->m_values[i][j]); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff3 ff3::operator*(ff3& m)
{
    ff3 r;

    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (int j = 0; j < ff3::m_dimension; j++)
        { r.set(i, j, this->row(i).dot(m.col(j))); }
    }

    return r;
}

//------------------------------------------------------------------------------

f3 ff3::operator*(f3& v)
{
    f3 r(0.0f, 0.0f, 0.0f);
    r.x = m_values[0][0] * v.x + m_values[0][1] * v.y + m_values[0][2] * v.z;
    r.y = m_values[1][0] * v.x + m_values[1][1] * v.y + m_values[1][2] * v.z;
    r.z = m_values[2][0] * v.x + m_values[2][1] * v.y + m_values[2][2] * v.z;
    return r;
}

//------------------------------------------------------------------------------

bool ff3::operator==(ff3& m)
{
    for (unsigned char i = 0; i < ff3::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff3::m_dimension; j++)
        {
            if (this->m_values[i][j] != m.get(i, j))
            { return false; }
        }
    }
    return true;
}