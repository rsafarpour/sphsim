﻿
#include "f4.h"

const f4 f4::O(0.0f, 0.0f, 0.0f, 0.0f);
const f4 f4::E(1.0f, 1.0f, 1.0f, 1.0f);
const f4 f4::UNIT_X(1.0f, 0.0f, 0.0f, 0.0f);
const f4 f4::UNIT_Y(0.0f, 1.0f, 0.0f, 0.0f);
const f4 f4::UNIT_Z(0.0f, 0.0f, 1.0f, 0.0f);
const f4 f4::UNIT_W(0.0f, 0.0f, 0.0f, 1.0f);

//-----------------------------------------------------------------------------

f4::f4()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
    w = 0.0f;
}

//-----------------------------------------------------------------------------

f4::f4(float x) 
  : f4()
{
    this->x = x;
}

//-----------------------------------------------------------------------------

f4::f4(float x, float y)
  : f4()
{
    this->x = x;
    this->y = y;
}

//-----------------------------------------------------------------------------

f4::f4(float x, float y, float z)
  : f4()
{
    this->x = x;
    this->y = y;
    this->z = z;
}

//-----------------------------------------------------------------------------

f4::f4(float x, float y, float z, float w)
  : f4()
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

//-----------------------------------------------------------------------------

float f4::dot(const f4& b) const
{
    float result;
    result = x * b.x + y * b.y + z * b.z + w * b.w;
    return result;
}

//-----------------------------------------------------------------------------

f4 f4::operator+(const f4& b) const
{
    f4 result;
    result.x = x + b.x;
    result.y = y + b.y;
    result.z = z + b.z;
    result.w = w + b.w;
    return result;
}

//-----------------------------------------------------------------------------

void  f4::operator+=(const f4& b)
{
    *this = *this + b;
}

//-----------------------------------------------------------------------------

f4 f4::operator-(const f4& b) const
{
    f4 result;
    result.x = x - b.x;
    result.y = y - b.y;
    result.z = z - b.z;
    result.w = w - b.w;
    return result;
}

//-----------------------------------------------------------------------------

void  f4::operator-=(const f4& b)
{
    *this = *this - b;
}

//-----------------------------------------------------------------------------

f4 f4::operator-() const
{
    f4 r;
    r.x = -this->x;
    r.y = -this->y;
    r.z = -this->z;
    r.w = -this->w;
    return r;
}

//-----------------------------------------------------------------------------

f4 f4::operator/(const f4& b) const
{
    f4 result;
    result.x = x / b.x;
    result.y = y / b.y;
    result.z = z / b.z;
    result.w = w / b.w;
    return result;
}

//-----------------------------------------------------------------------------

f4 f4::factor(float k)
{
    f4 result;
    result.x = k * x;
    result.y = k * y;
    result.z = k * z;
    result.w = k * w;
    return result;
}

//-----------------------------------------------------------------------------

f4 f4::times(f4& b)
{
    f4 result;
    result.x = b.x * x;
    result.y = b.y * y;
    result.z = b.z * z;
    result.w = b.w * w;
    return result;
}

//-----------------------------------------------------------------------------

bool f4::operator==(const f4& v) const
{
    if (!equal(x, v.x))
    { return false; }
    if (!equal(y, v.y))
    { return false; }
    if (!equal(z, v.z))
    { return false; }
    if (!equal(w, v.w))
    { return false; }

    {return true; }

}

//-----------------------------------------------------------------------------

bool f4::operator!=(const f4& v) const
{
    if (!equal(x, v.x))
    { return true; }
    if (!equal(y, v.y))
    { return true; }
    if (!equal(z, v.z))
    { return true; }
    if (!equal(w, v.w))
    { return true; }

    {return false; }
}

//-----------------------------------------------------------------------------

bool f4::isNull()
{
    if (!equal(x, 0.0f))
    { return false; }
    if (!equal(y, 0.0f))
    { return false; }
    if (!equal(z, 0.0f))
    { return false; }
    if (!equal(w, 0.0f))
    { return false; }

    {return true; }
}

//-----------------------------------------------------------------------------

void f4::operator=(float k)
{
    this->x = k;
    this->y = k;
    this->z = k;
    this->w = k;
}

//-----------------------------------------------------------------------------

void f4::length(float length)
{
    //x,y,z will be streched or shrunk by an factor to fit the size
    //this gets calculated with a vectors norm
    // v[norm] = 1 / ‖v‖
    //this needs to be multiplied with the desired length
    float norm = this->norm();

    if (equal(norm, 0.0f)) { return; }
    float factor = length / norm;

    x = x * factor;
    y = y * factor;
    z = z * factor;
    w = w * factor;
}

//-----------------------------------------------------------------------------

f4 f4::normal() const
{
    f4 r(*this);
    r.normalize();
    return r;
}

//-----------------------------------------------------------------------------

void f4::normalize()
{
    f4 tmp = this->factor(1 / this->norm());
    this->x = tmp.x;
    this->y = tmp.y;
    this->z = tmp.z;
    this->w = tmp.w;
}

//-----------------------------------------------------------------------------

f4 f4::inverse() const
{
    f4 r(*this);
    r.invert();
    return r;
}

//-----------------------------------------------------------------------------

void f4::invert()
{
    f4 r;
    this->x = -this->x;
    this->y = -this->y;
    this->z = -this->z;
    this->w = -this->w;
}

//-----------------------------------------------------------------------------

float f4::norm() const
{
    float normSquared = this->norm2();
    return sqrt(normSquared);
}

//-----------------------------------------------------------------------------

float f4::norm2() const
{
    //square all dimensions and sum them up
    return x*x + y*y + z*z + w*w;
}

//-----------------------------------------------------------------------------

f4 f4::to(const f4& pos) const
{
    return pos - *this;
}