﻿
#include "f3.h"

const f3 f3::O(0.0f, 0.0f, 0.0f);
const f3 f3::E(1.0f, 1.0f, 1.0f);
const f3 f3::UNIT_X(1.0f, 0.0f, 0.0f);
const f3 f3::UNIT_Y(0.0f, 1.0f, 0.0f);
const f3 f3::UNIT_Z(0.0f, 0.0f, 1.0f);

//-----------------------------------------------------------------------------

f3::f3()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}

//-----------------------------------------------------------------------------

f3::f3(float x)
{
    this->x = x;
}

//-----------------------------------------------------------------------------

f3::f3(float x, float y)
{
    this->x = x;
    this->y = y;
}

//-----------------------------------------------------------------------------

f3::f3(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

//-----------------------------------------------------------------------------

float f3::dot(const f3& b) const
{
    float result;
    result = x * b.x + y * b.y + z * b.z;
    return result;
}

//-----------------------------------------------------------------------------

f3 f3::cross(const f3& b) const
{
    f3 result;
    result.x = y * b.z - z * b.y;
    result.y = z * b.x - x * b.z;
    result.z = x * b.y - y * b.x;
    return result;
}

//-----------------------------------------------------------------------------

f3 f3::operator+(const f3& b) const
{
    f3 result;
    result.x = x + b.x;
    result.y = y + b.y;
    result.z = z + b.z;
    return result;
}

//-----------------------------------------------------------------------------

void  f3::operator+=(const f3& b)
{
    *this = *this + b;
}

//-----------------------------------------------------------------------------

f3 f3::operator-(const f3& b) const
{
    f3 result;
    result.x = x - b.x;
    result.y = y - b.y;
    result.z = z - b.z;
    return result;
}

//-----------------------------------------------------------------------------

void  f3::operator-=(const f3& b)
{
    *this = *this - b;
}

//-----------------------------------------------------------------------------

f3 f3::operator-() const
{
    f3 r;
    r.x = -this->x;
    r.y = -this->y;
    r.z = -this->z;
    return r;
}

//-----------------------------------------------------------------------------

f3 f3::operator/(const f3& b) const
{
    f3 result;
    result.x = x / b.x;
    result.y = y / b.y;
    result.z = z / b.z;
    return result;
}

//-----------------------------------------------------------------------------

f3 f3::factor(float k)
{
    f3 result;
    result.x = k * x;
    result.y = k * y;
    result.z = k * z;
    return result;
}

//-----------------------------------------------------------------------------

f3 f3::times(f3& b)
{
    f3 result;
    result.x = b.x * x;
    result.y = b.y * y;
    result.z = b.z * z;
    return result;
}

//-----------------------------------------------------------------------------

float f3::triple(f3& b, f3& c)
{
    return this->cross(b).dot(c);
}

//-----------------------------------------------------------------------------

bool f3::operator==(const f3& v) const
{
    if (!equal(x, v.x))
    { return false; }
    if (!equal(y, v.y))
    { return false; }
    if (!equal(z, v.z))
    { return false; }

    {return true; }

}

//-----------------------------------------------------------------------------

bool f3::operator!=(const f3& v) const
{
    if (!equal(x, v.x))
    { return true; }
    if (!equal(y, v.y))
    { return true; }
    if (!equal(z, v.z))
    { return true; }

    {return false; }
}

//-----------------------------------------------------------------------------

bool f3::isNull()
{
    if (!equal(x, 0.0))
    { return false; }
    if (!equal(y, 0.0))
    { return false; }
    if (!equal(z, 0.0))
    { return false; }

    {return true; }
}

//-----------------------------------------------------------------------------

void f3::operator=(float k)
{
    this->x = k;
    this->y = k;
    this->z = k;
}

//-----------------------------------------------------------------------------

void f3::length(float length)
{
    //x,y,z will be streched or buckled by an factor to fit the size
    //this gets calculated with a vectors norm
    // v[norm] = 1 / ‖v‖
    //this needs to be multiplied with the desired length
    float norm = this->norm();

    if (equal(norm, 0.0f)) { return; }
    float factor = length / norm;

    x = x * factor;
    y = y * factor;
    z = z * factor;
}

//-----------------------------------------------------------------------------

f3 f3::normal() const
{
    f3 r(this->x, this->y, this->z);
    r.normalize();
    return r;
}

//-----------------------------------------------------------------------------

void f3::normalize()
{
    float norm = this->norm();

    // This is an error, prevent 
    // at least a division by 0
    if (norm == 0)
    { norm = 1.0; }

    f3 tmp = this->factor(1 / norm);
    this->x = tmp.x;
    this->y = tmp.y;
    this->z = tmp.z;
}

//-----------------------------------------------------------------------------

f3 f3::inverse() const
{
    f3 r = *this;
    r.invert();
    return r;
}

//-----------------------------------------------------------------------------

void f3::invert()
{
    f3 r;
    this->x = -this->x;
    this->y = -this->y;
    this->z = -this->z;
}

//-----------------------------------------------------------------------------

float f3::norm2() const
{
    //square all dimensions and sum them up
    return x*x + y*y + z*z;
}

//-----------------------------------------------------------------------------

float f3::norm() const
{
    //square all dimensions and sum them up
    float normSquared = this->norm2();
    return sqrt(normSquared);

}


//-----------------------------------------------------------------------------

f3 f3::to(const f3& pos) const
{
    return pos - *this;
}