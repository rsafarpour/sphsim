
#ifndef MATHFUNCS_FF4
#define MATHFUNCS_FF4

#include <assert.h>

#include "ff3.h"
#include "f4.h"

class ff4
{

public:
    static const ff4 I;

    ff4();
    ff4(float _00, float _01, float _02, float _03,
        float _10, float _11, float _12, float _13,
        float _20, float _21, float _22, float _23,
        float _30, float _31, float _32, float _33);
    ff4(f4 r0, f4 r1, f4 r2, f4 r3);


    void  set(unsigned row, unsigned col, float value);
    float get(unsigned row, unsigned col);
    void  times(float factor);
    f4    row(unsigned char row);
    f4    col(unsigned char col);
    float det();
    ff4   transposed();
    void  transpose();
    ff4   adjugated();
    void  adjugate();
    ff4   inverse();
    void  invert();
    ff3   minor(unsigned row, unsigned col);

    ff4  operator+(ff4& m);
    ff4  operator-(ff4& m);
    ff4  operator-();
    ff4  operator*(ff4& m);
    f4   operator*(f4& v);
    bool operator==(ff4& m);
    bool operator!=(ff4& m);

private:
    static const int m_dimension = 4;
    float m_values[m_dimension][m_dimension];

};

#endif