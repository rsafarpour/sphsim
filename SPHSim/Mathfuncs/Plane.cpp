
#include "Plane.h"

Plane::Plane()
{
    this->m_coefficients.x = 0.0f;
    this->m_coefficients.y = 0.0f;
    this->m_coefficients.z = 0.0f;
    this->m_coefficients.w = 0.0f;
}

//-----------------------------------------------------------------------------

Plane::Plane(const f3& normal, const f3& point)
{
    assert(normal.norm() > 0.0f);

    this->m_coefficients.x = normal.x;
    this->m_coefficients.y = normal.y;
    this->m_coefficients.z = normal.z;
    this->m_coefficients.w = -point.dot(normal);
}

//-----------------------------------------------------------------------------

Plane& Plane::operator=(Plane& other)
{
    this->m_coefficients = other.m_coefficients;
    return *this;
}

//-----------------------------------------------------------------------------

float Plane::distance(const f3& point) const
{
    float retval = 0.0f;
    retval += this->m_coefficients.x * point.x;
    retval += this->m_coefficients.y * point.y;
    retval += this->m_coefficients.z * point.z;
    retval += this->m_coefficients.w;
    return retval;
}

//-----------------------------------------------------------------------------

f3 Plane::projection(const f3& point) const
{
    float  dist = this->distance(point);
    f3 normal = this->normal();


    return (point - normal.factor(dist));
}

//-----------------------------------------------------------------------------

f3 Plane::normal() const
{
    f3 normal;
    normal.x = this->m_coefficients.x;
    normal.y = this->m_coefficients.y;
    normal.z = this->m_coefficients.z;
    return normal;
}