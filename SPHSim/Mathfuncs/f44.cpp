
#include "ff4.h"

const ff4 ff4::I(1.0f, 0.0f, 0.0f, 0.0f,
                           0.0f, 1.0f, 0.0f, 0.0f,
                           0.0f, 0.0f, 1.0f, 0.0f,
                           0.0f, 0.0f, 0.0f, 1.0f);

//------------------------------------------------------------------------------

ff4::ff4()
{
    for (unsigned char i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff4::m_dimension; j++)
        { this->m_values[i][j] = 0.0f; }
    }
}

//------------------------------------------------------------------------------

ff4::ff4(
    float _00, float _01, float _02, float _03,
    float _10, float _11, float _12, float _13,
    float _20, float _21, float _22, float _23,
    float _30, float _31, float _32, float _33)
{
    this->m_values[0][0] = _00;
    this->m_values[0][1] = _01;
    this->m_values[0][2] = _02;
    this->m_values[0][3] = _03;
    this->m_values[1][0] = _10;
    this->m_values[1][1] = _11;
    this->m_values[1][2] = _12;
    this->m_values[1][3] = _13;
    this->m_values[2][0] = _20;
    this->m_values[2][1] = _21;
    this->m_values[2][2] = _22;
    this->m_values[2][3] = _23;
    this->m_values[3][0] = _30;
    this->m_values[3][1] = _31;
    this->m_values[3][2] = _32;
    this->m_values[3][3] = _33;
}

//------------------------------------------------------------------------------

void ff4::set(unsigned row, unsigned col, float value)
{
    assert(row < ff4::m_dimension);
    assert(col < ff4::m_dimension);

    this->m_values[row][col] = value;
}

//------------------------------------------------------------------------------

float ff4::get(unsigned row, unsigned col)
{
    assert(row < ff4::m_dimension);
    assert(col < ff4::m_dimension);

    return this->m_values[row][col];
}

//------------------------------------------------------------------------------

void ff4::times(float factor)
{
    for (unsigned char i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff4::m_dimension; j++)
        { this->m_values[i][j] *= factor; }
    }
}

//------------------------------------------------------------------------------

f4 ff4::row(unsigned char row)
{
    assert(row < ff4::m_dimension);

    f4 r;
    r.x = this->m_values[row][0];
    r.y = this->m_values[row][1];
    r.z = this->m_values[row][2];
    r.w = this->m_values[row][3];
    return r;
}

//------------------------------------------------------------------------------

f4 ff4::col(unsigned char col)
{
    assert(col < ff4::m_dimension);

    f4 r;
    r.x = this->m_values[0][col];
    r.y = this->m_values[1][col];
    r.z = this->m_values[2][col];
    r.w = this->m_values[3][col];
    return r;
}

//------------------------------------------------------------------------------

float ff4::det()
{
    float r = 0;
    ff3 minors[4];

    for (unsigned i = 0; i < ff4::m_dimension; i++)
    { minors[i] = this->minor(0, i); }

    r = m_values[0][0] * minors[0].det();
    r -=m_values[0][1] * minors[1].det();
    r +=m_values[0][2] * minors[2].det();
    r -=m_values[0][3] * minors[3].det();
    return r;
}

//------------------------------------------------------------------------------

ff4 ff4::transposed()
{
    ff4 r(*this);
    r.transpose();
    return r;
}

//------------------------------------------------------------------------------

void ff4::transpose()
{
    float temp[ff4::m_dimension][ff4::m_dimension];
    for (unsigned i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned j = 0; j < ff4::m_dimension; j++)
        { temp[j][i] = this->m_values[i][j]; }
    }

    std::swap(this->m_values, temp);
}

//------------------------------------------------------------------------------

ff4 ff4::adjugated()
{
    ff4 r(*this);
    r.adjugate();
    return r;
}

//------------------------------------------------------------------------------

void ff4::adjugate()
{
    ff4 cofactor;
    for (unsigned i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned j = 0; j < ff4::m_dimension; j++)
        {
            if ((i + j) % 2 == 0)
            { cofactor.m_values[i][j] = this->minor(i, j).det(); }
            else
            { cofactor.m_values[i][j] = -this->minor(i, j).det(); }
        }
    }

    ff4 adjugate = cofactor.transposed();
    std::swap(this->m_values, adjugate.m_values);
}

//------------------------------------------------------------------------------

ff4 ff4::inverse()
{
    ff4 r(*this);
    r.invert();
    return r;
}

//------------------------------------------------------------------------------

void ff4::invert()
{
    float det = this->det();

    assert(!equal(det, 0.0f));

    this->adjugate();
    this->times(1 / det);
}

//------------------------------------------------------------------------------

ff3 ff4::minor(unsigned row, unsigned col)
{
    assert(row < ff4::m_dimension);
    assert(col < ff4::m_dimension);


    ff3 r;
    int i_row = 0;
    for (unsigned i = 0; i < ff4::m_dimension; i++)
    {
        if (i == row) { continue; }

        int i_col = 0;
        for (unsigned j = 0; j < ff4::m_dimension; j++)
        {
            if (j == col) { continue; }
            r.set(i_row, i_col, this->m_values[i][j]);
            i_col++;
        }
        i_row++;
    }

    return r;
}

//------------------------------------------------------------------------------

ff4 ff4::operator+(ff4& m)
{
    ff4 r;

    for (int i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff4::m_dimension; j++)
        { r.set(i, j, this->m_values[i][j] + m.get(i, j)); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff4 ff4::operator-(ff4& m)
{
    ff4 r;
    for (unsigned char i = 0; i < ff4::m_dimension; i++)
    {
        for (int j = 0; j < ff4::m_dimension; j++)
        { r.set(i, j, this->m_values[i][j] + m.get(i, j)); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff4 ff4::operator-()
{
    ff4 r;
    for (unsigned char i = 0; i < ff4::m_dimension; i++)
    {
        for (int j = 0; j < ff4::m_dimension; j++)
        { r.set(i, j, -this->m_values[i][j]); }
    }
    return r;
}

//------------------------------------------------------------------------------

ff4 ff4::operator*(ff4& m)
{
    ff4 r;

    for (unsigned i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned j = 0; j < ff4::m_dimension; j++)
        { r.set(i, j, this->row(i).dot(m.col(j))); }
    }

    return r;
}

//------------------------------------------------------------------------------

f4 ff4::operator*(f4& v)
{
    f4 r;
    r.x = m_values[0][0] * v.x + m_values[0][1] * v.y + m_values[0][2] * v.z + m_values[0][3] * v.w;
    r.y = m_values[1][0] * v.x + m_values[1][1] * v.y + m_values[1][2] * v.z + m_values[1][3] * v.w;
    r.z = m_values[2][0] * v.x + m_values[2][1] * v.y + m_values[2][2] * v.z + m_values[2][3] * v.w;
    r.w = m_values[3][0] * v.x + m_values[3][1] * v.y + m_values[3][2] * v.z + m_values[3][3] * v.w;
    return r;
}

//------------------------------------------------------------------------------

bool ff4::operator==(ff4& m)
{
    for (unsigned char i = 0; i < ff4::m_dimension; i++)
    {
        for (unsigned char j = 0; j < ff4::m_dimension; j++)
        {
            if (this->m_values[i][j] != m.get(i, j))
            { return false; }
        }
    }
    return true;
}

//------------------------------------------------------------------------------

bool ff4::operator!=(ff4& m)
{
    return (!this->operator==(m));
}