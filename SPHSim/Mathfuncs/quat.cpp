
#include "quat.h"

quat quat::FromRotation(float angleRad, const f3& axis)
{

    if (equal(axis.x, axis.y) &&
        equal(axis.x, axis.z) &&
        equal(axis.x, 0.0f))
    {
        return quat(1.0f, 0.0f, 0.0f, 0.0f);
    }

    f3 normAxis = axis.normal();
    float sine = sin(angleRad / 2);
    float w = cos(angleRad / 2);
    float x = sine * normAxis.x;
    float y = sine * normAxis.y;
    float z = sine * normAxis.z;

    quat r(w, x, y, z);
    r.normalize();
    return r;
}

//------------------------------------------------------------------------------

quat quat::FromPoint(const f3& point)
{
    quat q;
    q.w = 0.0f;
    q.x = point.x;
    q.y = point.y;
    q.z = point.z;
    return q;
}

//------------------------------------------------------------------------------

quat::quat()
{
    this->w = 1.0f;
    this->x = 0.0f;
    this->y = 0.0f;
    this->z = 0.0f;
}

//------------------------------------------------------------------------------

quat::quat(float w, float x, float y, float z)
{
    this->w = w;
    this->x = x;
    this->y = y;
    this->z = z;
}

//------------------------------------------------------------------------------

quat quat::normal() const
{
    quat r(*this);
    r.normalize();
    return r;
}

//------------------------------------------------------------------------------

void quat::normalize()
{
    float w2 = this->w * this->w;
    float x2 = this->x * this->x;
    float y2 = this->y * this->y;
    float z2 = this->z * this->z;
    float mag = sqrt(w2 + x2 + y2 + z2);

    if (equal(mag, 0.0f))
    { return; }

    this->w = this->w / mag;
    this->x = this->x / mag;
    this->y = this->y / mag;
    this->z = this->z / mag;

    if (equal(0.0, this->w)) { this->w = 0.0f; };
    if (equal(0.0, this->x)) { this->x = 0.0f; };
    if (equal(0.0, this->y)) { this->y = 0.0f; };
    if (equal(0.0, this->z)) { this->z = 0.0f; };
}

//------------------------------------------------------------------------------

ff4 quat::matrix() const
{
    //reocurring calculations
    float xx = x*x;
    float xy = x*y;
    float xz = x*z;
    float xw = x*w;
    float yy = y*y;
    float yz = y*z;
    float yw = y*w;
    float zz = z*z;
    float zw = z*w;
    ff4 r;

    r.set(0, 0, 1 - 2 * (yy + zz));
    r.set(0, 1, 2 * (xy - zw));
    r.set(0, 2, 2 * (xz + yw));

    r.set(1, 0, 2 * (xy + zw));
    r.set(1, 1, 1 - 2 * (xx + zz));
    r.set(1, 2, 2 * (yz - xw));

    r.set(2, 0, 2 * (xz - yw));
    r.set(2, 1, 2 * (yz + xw));
    r.set(2, 2, 1 - 2 * (xx + yy));

    r.set(3, 3, 1.0f);
    return r;
}

//------------------------------------------------------------------------------

quat quat::conjugated() const
{
    quat q(*this);
    q.conjugate();
    return q;

}

//------------------------------------------------------------------------------

void quat::conjugate()
{
    this->w = this->w;
    this->x = -this->x;
    this->y = -this->y;
    this->z = -this->z;
}

//------------------------------------------------------------------------------

quat quat::inverse() const
{
    quat q(*this);
    q.invert();
    return q;
}

//------------------------------------------------------------------------------

void quat::invert()
{
    float normSq = w*w + x*x + y*y + z*z;
    this->conjugate();
    this->w /= normSq;
    this->x /= normSq;
    this->y /= normSq;
    this->z /= normSq; this->normalize();
}

//------------------------------------------------------------------------------

quat quat::operator*(const quat& q)
{
    quat r;
    r.w = w * q.w - x*q.x - y*q.y - z*q.z;
    r.x = w * q.x + x*q.w + y*q.z - z*q.y;
    r.y = w * q.y - x*q.z + y*q.w + z*q.x;
    r.z = w * q.z + x*q.y - y*q.x + z*q.w;
    return r;
}

//------------------------------------------------------------------------------

f3 quat::rotate(const f3& point)
{
    quat rotated = *this * quat::FromPoint(point) * this->conjugated();
    return f3(rotated.x, rotated.y, rotated.z);
}