
#ifndef MATHFUNCS_PLANE
#define MATHFUNCS_PLANE

#include <assert.h>

#include "f3.h"
#include "f4.h"

class Plane
{
public:
    Plane();
    Plane(const f3& normal, const f3& point);

    Plane& operator=(Plane& other);

    float  distance(const f3& point) const;
    f3 projection(const f3& point) const;
    f3 normal() const;

    f4 m_coefficients;
};

#endif