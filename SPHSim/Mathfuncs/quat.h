#ifndef MATHFUNCS_QUAT
#define MATHFUNCS_QUAT

#include "f3.h"
#include "ff4.h"

class quat
{

public:
    float x;
    float y;
    float z;
    float w;
    
    static quat FromRotation(float radians, const f3& axis);
    static quat FromPoint(const f3& point);
    
    quat();
    quat(float w, float x, float y, float z);
    
    quat normal() const;
    void normalize();
    ff4  matrix() const;
    quat conjugated() const;
    void conjugate();
    quat inverse() const;
    void invert();
    
    quat operator*(const quat& q);
    f3 rotate(const f3& point);

};

#endif