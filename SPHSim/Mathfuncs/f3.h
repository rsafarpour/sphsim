
#ifndef MATHFUNCS_f3
#define MATHFUNCS_f3

#include <math.h>

inline bool equal(float a, float b)
{
    static float epsilon = 0.0001f;
    return (fabs(a - b) < epsilon);
}

class f3
{
public:
    float x;
    float y;
    float z;
    
    //---------------------------------------------------------------------
    
    f3();
    f3(float x);
    f3(float x, float y);
    f3(float x, float y, float z);
    
    //---------------------------------------------------------------------
    
    // COMMON VECTORS:
    static const f3 O;
    static const f3 E;
    static const f3 UNIT_X;
    static const f3 UNIT_Y;
    static const f3 UNIT_Z;
    
    // OPERATORS:
    float dot(const f3& b) const;
    f3    cross(const f3& b) const;
    f3    operator+(const f3& b) const;
    void  operator+=(const f3& b);
    f3    operator-(const f3& b) const;
    void  operator-=(const f3& b);
    f3    operator-() const;
    f3    operator/(const f3& b) const;
    f3    factor(float k);
    f3    times(f3& v);
    float triple(f3& b, f3& c);
    bool  operator==(const f3& b) const;
    bool  operator!=(const f3& b) const;
    bool  isNull();
    void  operator=(float k);
    
    // SCALE:
    void  length(float length);
    f3    normal() const;
    void  normalize();
    f3    inverse() const;
    void  invert();
    float norm2()  const;
    float norm() const;
    
    f3 to(const f3& pos) const;
};

#endif