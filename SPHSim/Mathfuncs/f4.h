
#ifndef MATHFUNCS_F4
#define MATHFUNCS_F4

#include "f3.h"

class f4
{
public:
    float x;
    float y;
    float z;
    float w;



    f4();
    f4(float x);
    f4(float x, float y);
    f4(float x, float y, float z);
    f4(float x, float y, float z, float w);

    static const f4 O;
    static const f4 E;
    static const f4 UNIT_W;
    static const f4 UNIT_X;
    static const f4 UNIT_Y;
    static const f4 UNIT_Z;

    float dot(const f4& b) const;
    f4    operator+(const f4& b) const;
    void  operator+=(const f4& b);
    f4    operator-(const f4& b) const;
    void  operator-=(const f4& b);
    f4    operator-() const;
    f4    operator/(const f4& b) const;
    f4    factor(float k);
    f4    times(f4& v);
    bool  operator==(const f4& b) const;
    bool  operator!=(const f4& b) const;
    bool  isNull();
    void  operator=(float k);

    void  length(float length);
    f4    normal() const;
    void  normalize();
    f4    inverse() const;
    void  invert();
    float norm() const;
    float norm2() const;

    f4 to(const f4& pos) const;

};

#endif