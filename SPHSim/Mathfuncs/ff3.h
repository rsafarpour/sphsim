
#ifndef MATHFUNCS_FF3
#define MATHFUNCS_FF3

#include <utility>
#include "f3.h"

class ff3
{

public:
    static const ff3 I;


    ff3();
    ff3(float m00, float m01, float m02,
        float m10, float m11, float m12,
        float m20, float m21, float m22);
    ff3(f3 r0,
        f3 r1,
        f3 r2);

    void  set(unsigned row, unsigned col, float value);
    float get(unsigned row, unsigned col);
    void  times(float factor);
    f3    row(unsigned char row);
    f3    col(unsigned char col);
    float det();
    ff3   transposed();
    void  transpose();
    ff3   adjugated();
    void  adjugate();
    ff3   inverse();
    void  invert();

    ff3  operator+(ff3& m);
    ff3  operator-(ff3& m);
    ff3  operator-();
    ff3  operator*(ff3& m);
    f3   operator*(f3& v);
    bool operator==(ff3& m);
    bool operator!=(ff3& m);

private:
    static const int m_dimension = 3;

    float m_values[m_dimension][m_dimension];

};

#endif