
#include "Dbg.h"

namespace dbg
{

    void critical(const char* msg)
    {
        std::string str = "FATAL: ";
        str += msg;
        str += '\n';

        dbg::print(str);
        exit(-1);
    }

    //-------------------------------------------------------------------------

    void error(const char* msg)
    {
        std::string str = "ERROR: ";
        str += msg;
        str += '\n';

        dbg::print(str);
    }

    //-------------------------------------------------------------------------

    void warning(const char* msg)
    {
        std::string str = "WARNING: ";
        str += msg;
        str += '\n';

        dbg::print(str);
    }

    //-------------------------------------------------------------------------

    void critical(std::string& msg)
    { dbg::critical(msg.c_str()); }

    //-------------------------------------------------------------------------

    void error(std::string& msg)
    { dbg::error(msg.c_str()); }

    //-------------------------------------------------------------------------

    void warning(std::string& msg)
    { dbg::warning(msg.c_str()); }

    //-------------------------------------------------------------------------

    bool check_gl()
    {
        int err = glGetError();
        if (GL_NO_ERROR != err)
        {
            dbg::error((char*)gluErrorString(err));
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------

    bool check_sdl(int retval)
    {
        if (0 > retval)
        { 
            dbg::error(SDL_GetError());
            SDL_ClearError();
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------

    bool check_sdl(void* ptr)
    {
        if (!ptr)
        {
            dbg::error(SDL_GetError());
            SDL_ClearError();
            return true;
        }

        return false;
    }

    //-------------------------------------------------------------------------

    void print(std::string& dbgstr)
    { dbg::print(dbgstr.c_str()); }

    //-------------------------------------------------------------------------

    void print(const char* dbgstr)
    { 
        if (!dbgstr) return;

#ifdef __WIN32__
        OutputDebugString(dbgstr);
#else
        printf("%s\n", msg.c_str());
#endif
        fflush(stdout);
    }
}