
#include <cuda.h>
#include <cuda_runtime.h>

#include "Dbg.h"

#define check(func) cudaDbg(func, __FILE__, __LINE__);

void cudaDbg(cudaError_t err, const char* file, int line);
