
#ifndef SHELL_CONTENT
#define SHELL_CONTENT

#include <SDL.h>

#include "Input.h"

class RenderWindow;

class Content
{
public:
    virtual void Draw()                              = 0;
    virtual void Initialize(const RenderWindow& wnd) = 0;
    virtual bool Processed(SDL_Event& evt)           = 0;
    virtual void Terminate()                         = 0;
};

#endif