
#ifndef SHELL_MAINLOOP
#define SHELL_MAINLOOP

#include <SDL.h>

#include "RenderWindow.h"

class MainLoop final
{
public:
    MainLoop(Content* c);
    ~MainLoop();

    void          Init();
    RenderWindow& GetWindow();
    void          Run();
    void          Stop();

private:
    void poll();

    bool         m_runs;
    RenderWindow m_window;
};

#endif