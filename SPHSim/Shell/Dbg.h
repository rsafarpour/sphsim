
#ifndef SHELL_DBG
#define SHELL_DBG

#include <assert.h>
#include <SDL.h>
#include <stdio.h>
#include <string>
#include <glew.h>
#include <gl/GLU.h>
#include <Windows.h>

namespace dbg
{
    void critical(const char* msg);
    void error(const char* msg);
    void warning(const char* msg);
    void critical(std::string& msg);
    void error(std::string& msg);
    void warning(std::string& msg);

    bool check_gl();
    bool check_sdl(int retval);
    bool check_sdl(void* ptr);
    void print(std::string& dbgstr);
    void print(const char* dbgstr);
}

#endif 