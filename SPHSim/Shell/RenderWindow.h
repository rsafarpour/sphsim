#ifndef SHELL_RENDERWINDOW
#define SHELL_RENDERWINDOW

#include <SDL.h>
#include <SDL_video.h>
#include <memory>
#include <glew.h>
#include <gl/GL.h>

#include "Content.h"
#include "Dbg.h"
#include "Input.h"

class RenderWindow final
{
public:
    RenderWindow(Content* c);

    void Open();
    void Close();

    void          Draw();
    Content*      GetContent() const;
    SDL_GLContext GetContext() const;
    unsigned      GetWidth() const;
    unsigned      GetHeight() const;
    bool          HasID(unsigned id) const;
    void          SetWidth(unsigned w);
    void          SetHeight(unsigned h);
    void          SetTitle(const char* t);


private:

    void _mouse();
    void _update();

    std::unique_ptr<Content> m_content;
    SDL_GLContext            m_context;
    SDL_Window*              m_window;
    unsigned                 m_width;
    unsigned                 m_height;
    std::string              m_title;
};

#endif