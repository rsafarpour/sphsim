
#include "CudaDbg.h"

void cudaDbg(cudaError_t err, const char* file, int line)
{
    if (err != cudaSuccess)
    {
        std::string msg = "CUDA Error: ";
        msg += cudaGetErrorString(err);
        msg += "(File: ";
        msg += file;
        msg += ":" + std::to_string(line);
        msg += ")";

        dbg::error(msg);
    }
}