
#include "RenderWindow.h"

RenderWindow::RenderWindow(Content* c) 
  : m_content(c),
    m_width(100),
    m_height(100),
    m_title("")
{
    assert(c != nullptr);
}

//-----------------------------------------------------------------------------

void RenderWindow::SetWidth(unsigned w)
{
    this->m_width = w;
    this->_update();
}

//-----------------------------------------------------------------------------

void RenderWindow::SetHeight(unsigned h)
{
    this->m_height = h;
    this->_update();
}

//-----------------------------------------------------------------------------

void RenderWindow::SetTitle(const char* t)
{
    this->m_title = t;
    this->_update();
}

//-----------------------------------------------------------------------------

void RenderWindow::Open()
{
    if(!SDL_WasInit(SDL_INIT_VIDEO))
    { dbg::critical("Opening main window before video init"); }

    this->m_window = SDL_CreateWindow(
        this->m_title.c_str(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        this->m_width,
        this->m_height,
        SDL_WINDOW_OPENGL);

    dbg::check_sdl(this->m_window);
 
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);

    this->m_context = SDL_GL_CreateContext(this->m_window);
    dbg::check_sdl(this->m_context);
    
    glewExperimental = true;
    if (GLEW_OK != glewInit())
    { dbg::error("Failed initializing GLEW"); }
    
    // glew generates an error, 
    // this is a glew bug
    glGetError();

    this->m_content->Initialize(*this);
}

//-----------------------------------------------------------------------------

void RenderWindow::Close()
{
    this->m_content->Terminate();

    SDL_GL_DeleteContext(this->m_context);
    SDL_DestroyWindow(this->m_window);

    this->m_context = nullptr;
    this->m_window  = nullptr;
}

//-----------------------------------------------------------------------------

void RenderWindow::Draw()
{
    SDL_GL_MakeCurrent(this->m_window, this->m_context);
    this->m_content->Draw();
    SDL_GL_SwapWindow(this->m_window);
}

//-----------------------------------------------------------------------------

Content* RenderWindow::GetContent() const
{
    return this->m_content.get();
}

//-----------------------------------------------------------------------------

SDL_GLContext RenderWindow::GetContext() const
{
    return this->m_context;
}

//-----------------------------------------------------------------------------

unsigned RenderWindow::GetHeight() const
{ return this->m_height; }

//-----------------------------------------------------------------------------

unsigned RenderWindow::GetWidth() const
{ return this->m_width; }

//-----------------------------------------------------------------------------

bool RenderWindow::HasID(unsigned id) const
{
    return (SDL_GetWindowID(this->m_window) == id);
}

//-----------------------------------------------------------------------------

void RenderWindow::_update()
{
    SDL_SetWindowTitle(this->m_window, this->m_title.c_str());
    SDL_SetWindowSize(this->m_window, this->m_width, this->m_height);
}