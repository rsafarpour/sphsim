
#include "MainLoop.h"
#include "Dbg.h"

MainLoop::MainLoop(Content* c)
  : m_runs(true),
    m_window(c)
{}

//-------------------------------------------------------------------------

MainLoop::~MainLoop()
{
    this->m_window.Close();
}

//-------------------------------------------------------------------------

void MainLoop::Init()
{
    dbg::check_sdl(SDL_Init(SDL_INIT_VIDEO));
}

//-------------------------------------------------------------------------

RenderWindow& MainLoop::GetWindow()
{
    return this->m_window;
}

//-------------------------------------------------------------------------

void MainLoop::Run()
{
    this->m_window.Open();

    while (this->m_runs)
    { 
        this->poll(); 
        this->m_window.Draw();
    }
}

//-------------------------------------------------------------------------

void MainLoop::Stop()
{
    this->m_runs = false;
}

//-------------------------------------------------------------------------

void MainLoop::poll()
{
    Content* c = this->m_window.GetContent();

    SDL_Event evt;
    while (SDL_PollEvent(&evt))
    { 
        if (!c->Processed(evt))
        {
            if (evt.type == SDL_QUIT)
            { 
                SDL_Quit(); 
                this->m_runs = false;
            }
        }
    }
}